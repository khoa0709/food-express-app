package com.ute.food_express_app.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ute.food_express_app.R;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.model2.Feedback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ListFeedbackAdapter extends RecyclerView.Adapter<ListFeedbackAdapter.ListFeedbackViewHolder> {
    Context context;
    ArrayList<Feedback> listFeedback;



    public ListFeedbackAdapter(Context context, ArrayList<Feedback> listFeedback) {
        this.context = context;
        this.listFeedback = listFeedback;
    }

    @NonNull
    @Override
    public ListFeedbackAdapter.ListFeedbackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.item_feedback, parent, false);

        final ListFeedbackViewHolder listFeedbackViewHolder = new ListFeedbackViewHolder(view);


        return listFeedbackViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListFeedbackAdapter.ListFeedbackViewHolder holder, int position) {
        if(listFeedback.size() > 0){
            Feedback feedback = listFeedback.get(position);
            holder.username.setText(feedback.getUser().getName());
            holder.detail.setText(feedback.getDetail());

            if(feedback.getImages() != null)
                Picasso.get().load(feedback.getImages().get(0).getContent()).into(holder.imgReview1);

            DateFormat dateFormat = new SimpleDateFormat("hh:mm dd-mm-yyyy");
            holder.timeCreated.setText(dateFormat.format(feedback.getCreateTime()));
        }

//        holder.username.setText(itemFeedback.getUsername());
//        holder.timeCreated.setText(itemFeedback.getCreateTime());
//        holder.detail.setText(itemFeedback.getDetail());
//        holder.imgReview1.setImageResource(itemFeedback.getImageReview1());
//        holder.imgReview2.setImageResource(itemFeedback.getImageReview2());
//        holder.imgAvt.setImageResource(itemFeedback.getImage_avt());


    }

    @Override
    public int getItemCount() {
        return listFeedback.size();
    }

    class ListFeedbackViewHolder extends RecyclerView.ViewHolder {

        TextView username, timeCreated, detail;
        ImageView imgReview1, imgReview2, imgAvt;

        public ListFeedbackViewHolder(@NonNull View itemView) {
            super(itemView);
            username = (TextView) itemView.findViewById(R.id.feedback_username);
            timeCreated = (TextView) itemView.findViewById(R.id.feedback_time_created);
            detail = (TextView) itemView.findViewById(R.id.feedback_detail);
            imgReview1 = (ImageView) itemView.findViewById(R.id.feedback_img_review_1);
            imgAvt = (CircleImageView) itemView.findViewById(R.id.feedback_img_avt);




        }
    }
}
