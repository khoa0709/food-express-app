package com.ute.food_express_app.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.model.FoodCategory;
import com.ute.food_express_app.model.Restaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FoodCategoryAdapter extends RecyclerView.Adapter<FoodCategoryAdapter.FoodCategoryViewHolder> {

    private Context context;
    private List<FoodCategory> foodCategoryList;
    ListRestaurantAdapter listRestaurantAdapter;
    ArrayList<Restaurant> listRestaurant;

    public FoodCategoryAdapter(Context context, List<FoodCategory> foodCategoryList, ListRestaurantAdapter listRestaurantAdapter, ArrayList<Restaurant> listRestaurant) {
        this.context = context;
        this.foodCategoryList = foodCategoryList;
        this.listRestaurantAdapter = listRestaurantAdapter;
        this.listRestaurant = listRestaurant;
    }

    public void setData(List<FoodCategory> list){
        this.foodCategoryList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FoodCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food_category, parent, false);
        return new FoodCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodCategoryViewHolder holder, int position) {
        FoodCategory foodCategory = foodCategoryList.get(position);

        if(foodCategory == null)    return;

        holder.imgFoodCategoryLogo.setImageResource(foodCategory.getBackground());
        holder.txtFoodCategoryName.setText(foodCategory.getName());

        holder.imgFoodCategoryLogo.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onClick(View v) {
                listRestaurant.clear();
                getListRestaurantByMenuId(foodCategory.getId(), listRestaurant);

                final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listRestaurantAdapter.notifyDataSetChanged();
                    }
                }, 2000);


            }
        });
    }

    @Override
    public int getItemCount() {
        return foodCategoryList.size() > 0 ? foodCategoryList.size() : 0;
    }

    public class FoodCategoryViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgFoodCategoryLogo;
        private TextView txtFoodCategoryName;

        public FoodCategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            imgFoodCategoryLogo = itemView.findViewById(R.id.imgFoodCategoryLogo);
            txtFoodCategoryName = itemView.findViewById(R.id.txtFoodCategoryName);


        }
    }

    private void getListRestaurantByMenuId(int id, ArrayList<Restaurant> listRestaurant) {
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/restaurant/menuId/{menuId}")
                    .addPathParameter("menuId", String.valueOf(id))
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    Restaurant restaurant = new Restaurant();
                                    JSONObject obj = response.getJSONObject(i);
                                    restaurant.setId(obj.getInt("id"));
                                    restaurant.setName(obj.getString("nameRes"));
                                    restaurant.setOpenedTime(obj.getString("openedTime"));
                                    restaurant.setFinishedTime(obj.getString("finishedTime"));
                                    restaurant.setBackgroundUrl(obj.getString("background"));
                                    restaurant.setNumber(obj.getString("number"));
                                    restaurant.setStreet(obj.getString("street"));
                                    restaurant.setVillagedId(obj.getInt("villagedId"));
                                    restaurant.setMenuCategoryId(obj.getInt("menuCategoryId"));

                                    listRestaurant.add(restaurant);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }


                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
    }
}
