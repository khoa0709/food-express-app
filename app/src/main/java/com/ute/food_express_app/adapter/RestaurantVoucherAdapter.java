package com.ute.food_express_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ute.food_express_app.R;
import com.ute.food_express_app.fragment.UserFragment;
import com.ute.food_express_app.model.RestaurantVoucher;

import java.util.List;

public class RestaurantVoucherAdapter extends RecyclerView.Adapter<RestaurantVoucherAdapter.ResViewHolder>{
    private Context context;
    private List<RestaurantVoucher> restaurantVoucherList;

    public RestaurantVoucherAdapter(Context context){
        this.context = context;
    }
    public void setData(List<RestaurantVoucher> list){
        this.restaurantVoucherList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ResViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restaurantvoucher, parent, false);
        return new ResViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ResViewHolder holder, int position) {
        RestaurantVoucher restaurantVoucher = restaurantVoucherList.get(position);
        if (restaurantVoucher == null){
            return;
        }
        holder.img.setImageResource(restaurantVoucher.getImg());
        holder.tv_name.setText(restaurantVoucher.getName());
        holder.tv_address.setText(restaurantVoucher.getAddress());
        holder.tv_rate.setText(String.valueOf(restaurantVoucher.getRate()));
        holder.img.setImageResource(restaurantVoucher.getImg());
    }

    @Override
    public int getItemCount() {
        if(restaurantVoucherList != null){
            return restaurantVoucherList.size();
        }
        return 0;
    }

    public class ResViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;
        private TextView tv_name;
        private TextView tv_address;
        private TextView tv_rate;
        private TextView tv_location;

        public ResViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_res);
            tv_name = itemView.findViewById(R.id.name_res);
            tv_address = itemView.findViewById(R.id.des_res);
            tv_rate = itemView.findViewById(R.id.rate_res);
            tv_location = itemView.findViewById(R.id.location_res);
        }
    }

}
