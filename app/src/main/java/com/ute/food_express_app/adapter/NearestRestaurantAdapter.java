package com.ute.food_express_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ute.food_express_app.R;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.actvitie.RestaurantDetailActivity;
import com.ute.food_express_app.model.Restaurant;
import com.ute.food_express_app.model.SecondRestaurant;

import java.util.List;

public class NearestRestaurantAdapter extends RecyclerView.Adapter<NearestRestaurantAdapter.NearestRestaurantViewHolder> {

    private Context context;
    private List<Restaurant> restaurantList;

    public NearestRestaurantAdapter(Context context, List<Restaurant> restaurantList) {
        this.context = context;
        this.restaurantList = restaurantList;
    }

    public void setData(List<Restaurant> list){
        this.restaurantList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NearestRestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_second_restaurant, parent, false);
        return new NearestRestaurantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NearestRestaurantViewHolder holder, int position) {
        Restaurant restaurant = restaurantList.get(position);

        if(restaurant == null)
            return ;


        Picasso.get().load(restaurant.getBackgroundUrl()).into( holder.imgLogo);
        holder.txtRestaurantName.setText(restaurant.getName());
        holder.txtRate.setText("4.5");
        holder.txtDistance.setText("2");
        holder.txtTime.setText("10");

        holder.imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RestaurantDetailActivity.class);
                intent.putExtra("restaurantName", restaurant.getName());
                intent.putExtra("restaurantId", restaurant.getId());
                context.startActivity(intent);
            }
        });

        holder.txtRestaurantName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RestaurantDetailActivity.class);
                intent.putExtra("restaurantName", restaurant.getName());
                intent.putExtra("restaurantId", restaurant.getId());
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return restaurantList != null ? restaurantList.size() : 0;
    }

    public class NearestRestaurantViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgLogo;
        private TextView txtRestaurantName;
        private TextView txtRate;
        private TextView txtDistance;
        private TextView txtTime;

        public NearestRestaurantViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLogo = itemView.findViewById(R.id.imgLogo);
            txtRestaurantName = itemView.findViewById(R.id.txtName);
            txtRate = itemView.findViewById(R.id.txtRate);
            txtDistance = itemView.findViewById(R.id.txtDistance);
            txtTime = itemView.findViewById((R.id.txtTime));
        }
    }
}
