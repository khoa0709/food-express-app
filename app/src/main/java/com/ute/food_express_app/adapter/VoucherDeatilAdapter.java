package com.ute.food_express_app.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.ute.food_express_app.fragment.VoucherNearFragment;
import com.ute.food_express_app.fragment.VoucherVoteFragment;

public class VoucherDeatilAdapter extends FragmentStateAdapter {
    public VoucherDeatilAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                return new VoucherNearFragment();
            case 1:
                return new VoucherVoteFragment();
        }
        return new VoucherNearFragment();
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
