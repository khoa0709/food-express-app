package com.ute.food_express_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.ute.food_express_app.R;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.actvitie.RestaurantDetailActivity;
import com.ute.food_express_app.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

public class ListRestaurantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<Restaurant> listRestaurant;

    final int VIEW_TYPE_LOADING = 1;
    final int VIEW_TYPE_ITEM = 0;

    public ListRestaurantAdapter(Context context, ArrayList<Restaurant> listRestaurant) {
        this.context = context;
        this.listRestaurant = listRestaurant;
    }

//    public void setData(List<SecondRestaurant> list){
//        this.restaurantList = list;
//        notifyDataSetChanged();
//    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_list_restaurant_homepage, parent, false);
            return new RestaurantViewHolder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof RestaurantViewHolder) {
            setDataRestaurant((RestaurantViewHolder) holder, position);
        } else if(holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }


    }


    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        return listRestaurant.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return listRestaurant != null ? listRestaurant.size() : 0;
    }

    public class RestaurantViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgBackground;
        private TextView txtRestaurantName;
        private TextView txtNumber;
        private TextView txtStreet;
        private Button btnOrder;

        public RestaurantViewHolder(@NonNull View itemView) {
            super(itemView);

            imgBackground = itemView.findViewById(R.id.imgBackground);
            txtRestaurantName = itemView.findViewById(R.id.txtRestaurantName);
            txtNumber = itemView.findViewById(R.id.txtNumber);
            txtStreet = itemView.findViewById((R.id.txtStreet));
            btnOrder = itemView.findViewById(R.id.btnOrder);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;
        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }
    }

    void setDataRestaurant(RestaurantViewHolder holder, int position) {
        Restaurant restaurant = listRestaurant.get(position);
        if(restaurant != null) {
            Picasso.get().load(restaurant.getBackgroundUrl()).into(holder.imgBackground);
            holder.txtRestaurantName.setText(restaurant.getName());
            holder.txtNumber.setText(restaurant.getNumber() + " ");
            holder.txtStreet.setText(restaurant.getStreet());
            holder.btnOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, RestaurantDetailActivity.class);
                    intent.putExtra("restaurantName", restaurant.getName());
                    intent.putExtra("restaurantId", restaurant.getId());
                    context.startActivity(intent);
                }
            });
            holder.imgBackground.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, RestaurantDetailActivity.class);
                    intent.putExtra("restaurantName", restaurant.getName());
                    intent.putExtra("restaurantId", restaurant.getId());
                    context.startActivity(intent);
                }
            });
            holder.txtRestaurantName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, RestaurantDetailActivity.class);
                    intent.putExtra("restaurantName", restaurant.getName());
                    intent.putExtra("restaurantId", restaurant.getId());
                    context.startActivity(intent);
                }
            });
        } else {
            Log.d("error", "null");
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }
}
