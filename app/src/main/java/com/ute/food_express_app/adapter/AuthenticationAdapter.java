package com.ute.food_express_app.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.ute.food_express_app.fragment.Forgot_tab_fragment;
import com.ute.food_express_app.fragment.Login_tab_fragment;
import com.ute.food_express_app.fragment.Register_tab_fragment;

public class AuthenticationAdapter  extends FragmentStateAdapter{

    public AuthenticationAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new Login_tab_fragment();
            case 1:
                return new Register_tab_fragment();
            case 2:
                return new Forgot_tab_fragment();
        }
        return new Forgot_tab_fragment();
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}


