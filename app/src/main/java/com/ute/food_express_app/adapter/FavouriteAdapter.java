package com.ute.food_express_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ute.food_express_app.R;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.actvitie.RestaurantDetailActivity;
import com.ute.food_express_app.model.SecondRestaurant;

import java.util.List;

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.FavouriteViewHolder> {

    private Context context;
    private List<SecondRestaurant> restaurantList;

    public FavouriteAdapter(Context context, List<SecondRestaurant> restaurantList) {
        this.context = context;
        this.restaurantList = restaurantList;
    }

    public void setData(List<SecondRestaurant> list){
        this.restaurantList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FavouriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favourite, parent, false);
        return new FavouriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouriteViewHolder holder, int position) {
        SecondRestaurant restaurant = restaurantList.get(position);

        if(restaurant == null)
            return ;

        Picasso.get().load(restaurant.getImg()).into(holder.imgLogo);
        holder.txtRestaurantName.setText(restaurant.getName());
        holder.txtRate.setText(String.valueOf(restaurant.getRate()));
        holder.txtDistance.setText(String.valueOf(restaurant.getDistance()));
        holder.txtTime.setText(String.valueOf(restaurant.getTime()));
        holder.cardItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RestaurantDetailActivity.class);
                intent.putExtra("restaurantId", restaurant.getId());
                intent.putExtra("isFromFvt", 1);
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return restaurantList != null ? restaurantList.size() : 0;
    }

    public class FavouriteViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgLogo;
        private TextView txtRestaurantName;
        private TextView txtRate;
        private TextView txtDistance;
        private TextView txtTime;
        private ConstraintLayout cardItem;

        public FavouriteViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLogo = itemView.findViewById(R.id.imgLogo);
            txtRestaurantName = itemView.findViewById(R.id.txt_name);
            txtRate = itemView.findViewById(R.id.txtRate);
            txtDistance = itemView.findViewById(R.id.txtDistance);
            txtTime = itemView.findViewById((R.id.txtTime));
            cardItem = itemView.findViewById(R.id.card_item);
        }
    }
}
