package com.ute.food_express_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ute.food_express_app.R;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.actvitie.RestaurantDetailActivity;
import com.ute.food_express_app.model.Restaurant;

import java.util.List;

public class MostOrderRestaurantAdapter extends RecyclerView.Adapter<MostOrderRestaurantAdapter.RestaurantViewHolder> {
    private Context context;
    private List<Restaurant> restaurantList;

    public MostOrderRestaurantAdapter(Context context, List<Restaurant> restaurantList) {
        this.context = context;
        this.restaurantList = restaurantList;
    }

    public void setData(List<Restaurant> list) {
        this.restaurantList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_restaurant, parent, false);
        return new RestaurantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MostOrderRestaurantAdapter.RestaurantViewHolder holder, int position) {
        Restaurant restaurant = restaurantList.get(position);
        if (restaurant == null) {
            return;
        }
        // holder.imgView.setImageResource(restaurant.getRes_Id());
        Picasso.get().load(restaurant.getBackgroundUrl()).into(holder.imgView);
        holder.name.setText(restaurant.getName());
        holder.time.setText("Đã đặt 2 ngày trước");
        holder.btn_open_layout_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RestaurantDetailActivity.class);
                intent.putExtra("restaurantName", restaurant.getName());
                intent.putExtra("restaurantId", restaurant.getId());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        if (restaurantList != null) {
            return restaurantList.size();
        }
        return 0;
    }

    public class RestaurantViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgView;
        private TextView name;
        private TextView time;
        private Button btn_open_layout_detail;

        public RestaurantViewHolder(@NonNull View itemView) {
            super(itemView);

            imgView = itemView.findViewById(R.id.imageView);
            name = itemView.findViewById(R.id.name);
            time = itemView.findViewById(R.id.time);
            btn_open_layout_detail = itemView.findViewById(R.id.btn_open_layout_detail);
        }
    }
}
