package com.ute.food_express_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.ute.food_express_app.R;
import com.ute.food_express_app.model.ImageSlide;

import java.util.List;

public class ImageSlideAdapter extends PagerAdapter {
    private Context context;
    private List<ImageSlide> imageSlideList;

    public ImageSlideAdapter(Context context, List<ImageSlide> imageSlideList) {
        this.context = context;
        this.imageSlideList = imageSlideList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.item_imageslide, container, false);
        ImageView imgSlide = view.findViewById(R.id.image_slide);

        ImageSlide imageSlide = imageSlideList.get(position);
        if(imageSlide != null){
            Glide.with(context).load(imageSlide.getRes_id()).into(imgSlide);
        }
        //add View to ViewGroup
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        if(imageSlideList != null){
            return imageSlideList.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //Remove view
        container.removeView((View) object);
    }
}
