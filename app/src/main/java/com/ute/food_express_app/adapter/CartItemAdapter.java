package com.ute.food_express_app.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.R;
import com.ute.food_express_app.model.CartItemModel;
import com.ute.food_express_app.model2.Restaurant;
import com.ute.food_express_app.model.SecondRestaurant;
import com.ute.food_express_app.model2.Food;
import com.ute.food_express_app.model2.Order;
import com.ute.food_express_app.model2.OrderDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.CartItemViewHolder> {
    private Context context;
    private List<Order> orderList;
    private Restaurant restaurant;

    public CartItemAdapter(Context context, List<Order> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    public void setData(List<Order> list){
        this.orderList = list;
    }

    @NonNull
    @Override
    public CartItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        return new CartItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartItemAdapter.CartItemViewHolder holder, int position) {
        Order order = orderList.get(position);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(order==null) return;

                restaurant = order.getRestaurant();

                if(restaurant != null){
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

                    holder.txtDate.setText(format.format(order.getCreatedTime()));
                    Picasso.get().load(restaurant.getBackground()).into(holder.imgLogo);
                    holder.txtName.setText(restaurant.getName());
                    holder.txtPrice.setText(getTotalCost(order));
                    holder.txtNumber.setText(getQuantity(order) + " phần");
                    holder.txtAddress.setText(restaurant.getNumber() + " " + restaurant.getStreet());
                }


//              holder.txtPayment.setText(cartItem.getPayment());
                if(order.getOrderStatus() == 6) {
                    holder.status.setVisibility(View.INVISIBLE);
                    holder.statusCancel.setVisibility(View.VISIBLE);
                }
                if(order.getOrderStatus() == 5) {
                    holder.status.setVisibility(View.VISIBLE);
                    holder.statusCancel.setVisibility(View.INVISIBLE);
                }
            }
        }, 1500);

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class CartItemViewHolder extends RecyclerView.ViewHolder{

        private TextView txtDate;
        private ImageView imgLogo;
        private TextView txtName;
        private TextView txtAddress;
        private TextView txtPrice;
        private TextView txtNumber;
        private TextView txtPayment;
        private LinearLayout status, statusCancel;


        public CartItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtDate = itemView.findViewById(R.id.txt_date);
            imgLogo = itemView.findViewById(R.id.img_logo);
            txtName = itemView.findViewById(R.id.txt_name);
            txtAddress = itemView.findViewById(R.id.txt_address);
            txtPrice = itemView.findViewById(R.id.txt_price);
            txtNumber = itemView.findViewById((R.id.txt_number));
            status = itemView.findViewById((R.id.status));
            statusCancel = itemView.findViewById(R.id.status_cancel);
        }
    }



    public int getQuantity(Order order){
        int all = 0;

        for(OrderDetail orderDetail : order.getOrderDetails()){
            all += orderDetail.getQuantity();
        }

        return all;
    }

    public String getTotalCost(Order order){
        int all = 0;

        for(OrderDetail orderDetail : order.getOrderDetails()){
            all += orderDetail.getPrice();
        }

        all += order.getDeliveryCost();

        DecimalFormat formatter = new DecimalFormat("###,###,###");


        return formatter.format(all)+" đ" ;
    }
}
