package com.ute.food_express_app.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ute.food_express_app.R;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.actvitie.OrderActivity;
import com.ute.food_express_app.fragment.CartFragment;
import com.ute.food_express_app.model.Food;

import java.text.NumberFormat;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.PropertyPermission;

public class ListFoodAdapter extends RecyclerView.Adapter<ListFoodAdapter.ListFoodViewHolder> {

    Context context;
    ArrayList<Food> listFood;
    ConstraintLayout layoutCart;
    ConstraintLayout layoutCartTemporary;
    ImageButton btn_cartTemp;
    TextView tv_totalMoney, btn_removeAll;
    Button btn_delivery;
    RecyclerView rcv_listFoodCartTemp;
    ImageView btn_closeCartTemp;
    String restaurantName;
    DecimalFormat formatter = new DecimalFormat("###,###,###");

    ArrayList<Food> listFoodTemp = new ArrayList<>();
    CartFoodAdapter cartFoodAdapter;
    float v = 0;

    int quantity = 0;
    int totalMoney = 0;

    public ListFoodAdapter(Context context, ArrayList<Food> listFood, ConstraintLayout layoutCart,
            ConstraintLayout layoutCartTemporary) {
        this.context = context;
        this.listFood = listFood;
        this.layoutCart = layoutCart;
        this.layoutCartTemporary = layoutCartTemporary;
    }

    @NonNull
    @Override
    public ListFoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        layoutCart.setVisibility(View.VISIBLE);
        layoutCartTemporary.setVisibility(View.VISIBLE);

        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("data",
                Context.MODE_PRIVATE);
        restaurantName = sharedPreferences.getString("restaurantName", "Pizza Hot");

        btn_cartTemp = layoutCart.findViewById(R.id.btn_cartTemp);
        btn_delivery = layoutCart.findViewById(R.id.btn_delivery);
        tv_totalMoney = layoutCart.findViewById(R.id.tv_totalMoney);
        rcv_listFoodCartTemp = layoutCartTemporary.findViewById(R.id.rcv_listFoodCartTemp);
        btn_removeAll = layoutCartTemporary.findViewById(R.id.btn_removeAll);
        btn_closeCartTemp = layoutCartTemporary.findViewById(R.id.btn_closeCartTemp);

        btn_cartTemp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cartFoodAdapter = new CartFoodAdapter(context, listFoodTemp);
                rcv_listFoodCartTemp.setLayoutManager(new LinearLayoutManager(context));
                rcv_listFoodCartTemp.setAdapter(cartFoodAdapter);
                layoutCartTemporary.animate().translationY(0).alpha(1).setDuration(500).start();
            }
        });

        btn_closeCartTemp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutCartTemporary.animate().translationY(600).alpha(0).setDuration(500).start();
                layoutCartTemporary.clearFocus();
                layoutCart.clearFocus();
                if (totalMoney == 0) {
                    hideLayoutCart();
                }

            }
        });

        btn_removeAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listFoodTemp.clear();
                cartFoodAdapter.notifyDataSetChanged();
                totalMoney = 0;

            }
        });

        btn_delivery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context.getApplicationContext(), OrderActivity.class);
                intent.putExtra("foodList", (Serializable) listFoodTemp);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.getApplicationContext().startActivity(intent);
            }
        });

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_food, parent, false);
        final ListFoodViewHolder listFoodViewHolder = new ListFoodViewHolder(view);
        return listFoodViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListFoodViewHolder holder, int position) {
        Food itemFood = listFood.get(position);
        holder.name.setText(itemFood.getName());

        NumberFormat formatter = new DecimalFormat("#,###");
        String formattedNumber = formatter.format(itemFood.getPrice());
        // NumberFormat formatter = new DecimalFormat("#,###");
        // String priceFormat = formatter.format(itemFood.getPrice().toString());
        holder.price.setText(formatter.format(itemFood.getPrice()));
        Picasso.get().load(itemFood.getImageUrl()).into(holder.img);

        // holder.

        setTranslation(holder);
        setAlpha(holder);

        holder.btn_increase.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity += 1;
                totalMoney += itemFood.getPrice();
                itemFood.setQuantityOrder(itemFood.getQuantityOrder() + 1);
                holder.tv_quantity.setText(itemFood.getQuantityOrder().toString());
                holder.tv_quantity.animate().translationX(-50).alpha(1).setDuration(500).start();
                holder.btn_decrease.animate().translationX(-150).alpha(1).setDuration(700).start();
                displayLayoutCart();
                NumberFormat formatter = new DecimalFormat("#,###");
                String formattedNumber = formatter.format(totalMoney);
                tv_totalMoney.setText(formattedNumber + " VNĐ");
                if (itemFood.getQuantityOrder() == 1) {
                    addToListFoodTemp(itemFood, listFoodTemp);
                } else {
                    updateItemInListFoodTemp(itemFood, listFoodTemp);
                }
            }
        });

        holder.btn_decrease.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                quantity -= 1;
                totalMoney -= itemFood.getPrice();
                itemFood.setQuantityOrder(itemFood.getQuantityOrder() - 1);
                holder.tv_quantity.setText(itemFood.getQuantityOrder().toString());
                if (itemFood.getQuantityOrder() == 0) {
                    holder.tv_quantity.animate().translationX(0).alpha(0).setDuration(500).start();
                    holder.btn_decrease.animate().translationX(0).alpha(0).setDuration(700).start();
                }
                if (quantity == 0) {
                    hideLayoutCart();
                }
                NumberFormat formatter = new DecimalFormat("#,###");
                String formattedNumber = formatter.format(totalMoney);
                tv_totalMoney.setText(formattedNumber + " VNĐ");
                if (itemFood.getQuantityOrder() == 0) {
                    removeItemInListFoodTemp(itemFood, listFoodTemp);
                } else {
                    updateItemInListFoodTemp(itemFood, listFoodTemp);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFood.size();
    }

    class ListFoodViewHolder extends RecyclerView.ViewHolder {

        TextView name, price, tv_quantity;
        ImageView img;
        Button btn_increase, btn_decrease;
        View container;
        LinearLayout layoutItem;

        public ListFoodViewHolder(@NonNull View itemView) {
            super(itemView);
            container = itemView;
            name = (TextView) itemView.findViewById(R.id.food_name);
            price = (TextView) itemView.findViewById(R.id.food_price);
            img = (ImageView) itemView.findViewById(R.id.food_img);
            btn_increase = (Button) itemView.findViewById(R.id.btn_increase);
            btn_decrease = (Button) itemView.findViewById(R.id.btn_decrease);
            tv_quantity = (TextView) itemView.findViewById(R.id.tv_quantity);
            layoutItem = (LinearLayout) itemView.findViewById(R.id.food_layout_item);
        }
    }

    private void openCartTemporary(int gravity) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cart_temporary);

        Window window = dialog.getWindow();
        if (window == null) {
            return;
        }

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = gravity;
        window.setAttributes(windowAttributes);

        ImageButton btnAddCart = (ImageButton) dialog.findViewById(R.id.btnAddCart);
        btnAddCart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                AppCompatActivity activity = (AppCompatActivity) context;
                Fragment cartFragment = new CartFragment();
                FragmentManager fragmentManager = activity.getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                // fragmentTransaction.replace(R.id.layoutCart, cartFragment);
                fragmentTransaction.commit();

            }
        });

        dialog.show();
    }

    private void setTranslation(@NonNull ListFoodViewHolder holder) {
        holder.btn_decrease.setTranslationX(0);
        holder.tv_quantity.setTranslationX(0);
        layoutCart.setTranslationY(200);
        layoutCartTemporary.setTranslationY(600);
    }

    private void setAlpha(@NonNull ListFoodViewHolder holder) {
        holder.btn_decrease.setAlpha(v);
        holder.tv_quantity.setAlpha(v);
        layoutCart.setAlpha(v);
        layoutCartTemporary.setAlpha(v);
    }

    private void addToListFoodTemp(Food itemFood, ArrayList<Food> listFoodTemp) {
        listFoodTemp.add(itemFood);
    }

    private void updateItemInListFoodTemp(Food itemFood, ArrayList<Food> listFoodTemp) {
        for (int i = 0; i < listFoodTemp.size(); i++) {
            if (itemFood.getId() == listFoodTemp.get(i).getId()) {
                listFoodTemp.get(i).setQuantityOrder(itemFood.getQuantityOrder());
            }
        }
    }

    private void removeItemInListFoodTemp(Food itemFood, ArrayList<Food> listFoodTemp) {
        listFoodTemp.remove(itemFood);
    }

    private void displayLayoutCart() {
        layoutCart.animate().translationY(-120).alpha(1).setDuration(500).start();
    }

    private void hideLayoutCart() {
        layoutCart.animate().translationY(300).alpha(0).setDuration(500).start();
    }
}
