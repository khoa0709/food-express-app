package com.ute.food_express_app.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ute.food_express_app.R;
import com.ute.food_express_app.model.Food;
import com.ute.food_express_app.model.MyFood;
import com.ute.food_express_app.model2.User;

import java.text.DecimalFormat;
import java.util.List;


public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderFoodViewHolder>{

    private Context context;
    private List<Food> foodList;
    String restaurantName;


    DecimalFormat formatter = new DecimalFormat("###,###,###");

    public OrderDetailAdapter(Context context, List<Food> foodList){
        this.context = context;
        this.foodList = foodList;
    }

    @NonNull
    @Override
    public OrderFoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("data", Context.MODE_PRIVATE);
        restaurantName = sharedPreferences.getString("restaurantName", "Pizza Hot");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food_after_order, parent, false);
        return new OrderFoodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderFoodViewHolder holder, int position) {
        Food food = foodList.get(position);

        if(food == null) return;

        Picasso.get().load(food.getImageUrl()).into(holder.imgFood);
        holder.txtFoodName.setText(food.getName());
        holder.txtRestaurantName.setText(restaurantName);
        holder.txtPrice.setText(formatter.format(food.getPrice()));
        holder.txtQuantity.setText(String.valueOf(food.getQuantityOrder()));

    }

    @Override
    public int getItemCount() {
        return foodList.size() > 0 ? foodList.size() : 0;
    }


    public class OrderFoodViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgFood;
        private TextView txtFoodName, txtRestaurantName, txtPrice, txtQuantity;

        public OrderFoodViewHolder(@NonNull View itemView) {
            super(itemView);

            imgFood = itemView.findViewById(R.id.imgFoodImage);
            txtFoodName = itemView.findViewById(R.id.txtFoodName);
            txtRestaurantName = itemView.findViewById(R.id.txtRestaurantName);
            txtPrice = itemView.findViewById(R.id.txtFoodPrice);
            txtQuantity = itemView.findViewById(R.id.txtQuantity);
        }
    }
}
