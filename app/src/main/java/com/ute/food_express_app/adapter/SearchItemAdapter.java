package com.ute.food_express_app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ute.food_express_app.R;
import com.ute.food_express_app.model.Restaurant;
import com.ute.food_express_app.model.SecondRestaurant;

import java.util.List;

public class SearchItemAdapter extends RecyclerView.Adapter<SearchItemAdapter.SearchItemViewHolder> {

    private Context context;
    private List<Restaurant> restaurantList;

    public SearchItemAdapter(Context context, List<Restaurant> restaurantList) {
        this.context = context;
        this.restaurantList = restaurantList;
    }

    public void setData(List<Restaurant> list){
        this.restaurantList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SearchItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listsearch, parent, false);
        return new SearchItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchItemViewHolder holder, int position) {
        Restaurant restaurant = restaurantList.get(position);

        if(restaurant == null)
            return ;

//        holder.imgLogo.setImageResource(restaurant.getId());
        Picasso.get().load(restaurant.getBackgroundUrl()).into(holder.imgLogo);
        holder.txtRestaurantName.setText(restaurant.getName());
        holder.txtRate.setText("4.0 (100+)");
        holder.txtDistance.setText("2 km");
        holder.txtAddress.setText(restaurant.getNumber() + " " + restaurant.getStreet());

    }


    @Override
    public int getItemCount() {
        return restaurantList != null ? restaurantList.size() : 0;
    }

    public class SearchItemViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgLogo;
        private TextView txtRestaurantName;
        private TextView txtRate;
        private TextView txtDistance;
        private TextView txtAddress;

        public SearchItemViewHolder(@NonNull View itemView) {
            super(itemView);

            imgLogo = itemView.findViewById(R.id.image);
            txtRestaurantName = itemView.findViewById(R.id.name);
            txtRate = itemView.findViewById(R.id.txtRate);
            txtDistance = itemView.findViewById(R.id.txtDistance);
            txtAddress = itemView.findViewById(R.id.address);
        }
    }
}
