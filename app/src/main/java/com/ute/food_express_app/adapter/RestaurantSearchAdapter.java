package com.ute.food_express_app.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.R;
import com.ute.food_express_app.actvitie.ListSearchResActivity;
import com.ute.food_express_app.actvitie.RestaurantDetailActivity;
import com.ute.food_express_app.model.Restaurant;
import com.ute.food_express_app.model.RestaurantSearch;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RestaurantSearchAdapter extends RecyclerView.Adapter<RestaurantSearchAdapter.RestaurantViewHolder> {
    private List<Restaurant> mListRes;
    private List<Restaurant> mListResFull;
    private Context context;
    public RestaurantSearchAdapter(Context context, List<Restaurant> mListRes) {
        this.mListRes = mListRes;
        this.context = context;
    }

    public void setData(List<Restaurant> list){
        this.mListRes = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RestaurantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_listsearch,parent,false);
        return new RestaurantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RestaurantViewHolder holder, int position) {
        Restaurant restaurant = mListRes.get(position);
        if (restaurant == null){
            return;
        }
        Picasso.get().load(restaurant.getBackgroundUrl()).into(holder.imgRes);
        holder.nameRes.setText(restaurant.getName());
        holder.addRes.setText(restaurant.getNumber() + " " + restaurant.getStreet());

        holder.layoutSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RestaurantDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("restaurantName", restaurant.getName());
                intent.putExtra("restaurantId", restaurant.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mListRes != null){
            return mListRes.size();
        }
        return 0;
    }

    public class RestaurantViewHolder extends RecyclerView.ViewHolder{
        LinearLayout layoutSearch;
        ImageView imgRes;
        TextView nameRes;
        TextView addRes;
        public RestaurantViewHolder(@NonNull View itemView) {
            super(itemView);
            imgRes = itemView.findViewById(R.id.image);
            nameRes = itemView.findViewById(R.id.name);
            addRes = itemView.findViewById(R.id.address);
            layoutSearch = itemView.findViewById(R.id.layoutSearchRestaurant);
        }
    }
}
