package com.ute.food_express_app.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.ute.food_express_app.fragment.FavouriteNearestFragment;
import com.ute.food_express_app.fragment.FavouriteRecentFragment;

public class FavouriteListAdapter extends FragmentStateAdapter {

    public FavouriteListAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle ){
        super(fragmentManager, lifecycle);

    }

    @NonNull
    @Override

    public Fragment createFragment(int position) {
        switch (position) {
            case 0: return new FavouriteRecentFragment();
            case 1: return new FavouriteNearestFragment();
        }
        return new FavouriteRecentFragment();
    }


    @Override
    public int getItemCount() {
        return 2;
    }


}
