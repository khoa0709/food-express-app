package com.ute.food_express_app.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.DataSetObservable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ute.food_express_app.R;
import com.ute.food_express_app.actvitie.OrderActivity;
import com.ute.food_express_app.model.Food;
import com.ute.food_express_app.model.MyFood;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class CartFoodAdapter extends RecyclerView.Adapter<CartFoodAdapter.CartFoodViewHolder> {

    private Context context;
    ArrayList<Food> listFoodTemp;
    String restaurantName;
//    DecimalFormat formatter = new DecimalFormat("###,###,###");

    public CartFoodAdapter(Context context, ArrayList<Food> listFoodTemp) {
        this.context = context;
        this.listFoodTemp = listFoodTemp;
    }

    @NonNull
    @Override
    public CartFoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("data",
                Context.MODE_PRIVATE);
        restaurantName = sharedPreferences.getString("restaurantName", "Pizza Hot");

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_food, parent, false);
        return new CartFoodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartFoodViewHolder holder, int position) {
        Food food = listFoodTemp.get(position);

        if (food == null)
            return;

        // holder.imgFood.setImageResource(food.getId());
        Picasso.get().load(food.getImageUrl()).into(holder.imgFood);
        holder.txtFoodName.setText(food.getName());
        // holder.txtRestaurantName.setText(food.getRestaurantName());
        NumberFormat formatter = new DecimalFormat("#,###");
        String formattedNumber = formatter.format(food.getPrice());
        holder.txtRestaurantName.setText(restaurantName);
        holder.txtPrice.setText(formattedNumber + "VNĐ");
        holder.edtQuantity.setText(String.valueOf(food.getQuantityOrder()));

        holder.btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                food.setQuantityOrder(food.getQuantityOrder() - 1);
                holder.edtQuantity.setText(String.valueOf(food.getQuantityOrder()));
                if (food.getQuantityOrder() == 0) {
                    listFoodTemp.remove(food);
                    notifyDataSetChanged();
                }
                if (context instanceof OrderActivity)
                    ((OrderActivity) context).setValue();
            }
        });

        holder.btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                food.setQuantityOrder(food.getQuantityOrder() + 1);
                holder.edtQuantity.setText(String.valueOf(food.getQuantityOrder()));
                if (context instanceof OrderActivity)
                    ((OrderActivity) context).setValue();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listFoodTemp.size() > 0 ? listFoodTemp.size() : 0;
    }

    public void registerAdapterDataObserver(DataSetObservable dataSetObservable) {
    }

    public class CartFoodViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgFood;
        private TextView txtFoodName, txtRestaurantName, txtPrice, edtQuantity;
        private Button btnDecrease, btnIncrease;

        public CartFoodViewHolder(@NonNull View itemView) {
            super(itemView);

            imgFood = itemView.findViewById(R.id.imgFoodImage);
            txtFoodName = itemView.findViewById(R.id.txtFoodName);
            txtRestaurantName = itemView.findViewById(R.id.txtRestaurantName);
            txtPrice = itemView.findViewById(R.id.txtFoodPrice);
            edtQuantity = itemView.findViewById(R.id.edtQuantity);
            btnDecrease = itemView.findViewById(R.id.btnDecrease);
            btnIncrease = itemView.findViewById(R.id.btnIncrease);
        }
    }
}
