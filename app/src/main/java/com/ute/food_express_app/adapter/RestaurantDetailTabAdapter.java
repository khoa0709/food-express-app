package com.ute.food_express_app.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.ute.food_express_app.fragment.Comment_res_fragment;
import com.ute.food_express_app.fragment.InforRes_fragment;
import com.ute.food_express_app.fragment.Set_Menu_Fragment;
import com.ute.food_express_app.model.Food;

import java.util.ArrayList;

public class RestaurantDetailTabAdapter extends FragmentStateAdapter {

    Context context;
    ConstraintLayout layoutCart;
    ConstraintLayout layoutCartTemporary;
//    RecyclerView rcv_listFoodCartTemp;
//    CartFoodAdapter cartFoodAdapter;
//    ArrayList<Food> listFoodTemp;

    public RestaurantDetailTabAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, @NonNull Context context,
                                      @NonNull ConstraintLayout layoutCart, @NonNull ConstraintLayout layoutCartTemporary) {
        super(fragmentManager, lifecycle);
        this.context = context;
        this.layoutCart = layoutCart;
        this.layoutCartTemporary = layoutCartTemporary;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new Set_Menu_Fragment(context, layoutCart, layoutCartTemporary);
            case 1:
                return new Comment_res_fragment();
            case 2:
                return new InforRes_fragment();
        };
        return new InforRes_fragment();
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
