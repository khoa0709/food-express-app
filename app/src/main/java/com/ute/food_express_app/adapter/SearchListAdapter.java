package com.ute.food_express_app.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.ute.food_express_app.fragment.AllSearchFragment;
import com.ute.food_express_app.fragment.RateSearchFragment;

public class SearchListAdapter extends FragmentStateAdapter {

    String id;

    public SearchListAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, String id) {
        super(fragmentManager, lifecycle);
        this.id = id;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new AllSearchFragment(id);
            case 1:
                return new RateSearchFragment(id);
        }
        return new AllSearchFragment(id);
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
