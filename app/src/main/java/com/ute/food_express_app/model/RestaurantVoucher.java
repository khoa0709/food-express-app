package com.ute.food_express_app.model;

public class RestaurantVoucher {
    int img;
    String name;
    String address;
    double rate;
    String location;

    public RestaurantVoucher(int img, String name, String address, double rate, String location) {
        this.img = img;
        this.name = name;
        this.address = address;
        this.rate = rate;
        this.location = location;
    }

    public int getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public double getRate() {
        return rate;
    }

    public String getLocation() {
        return location;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
