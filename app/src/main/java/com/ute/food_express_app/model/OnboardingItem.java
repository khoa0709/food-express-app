package com.ute.food_express_app.model;

public class OnboardingItem {
    private int image_logo;
    private int image_description;
    private String title;
    private String description;

    public OnboardingItem(int image_logo, int image_description, String title, String description) {
        this.image_logo = image_logo;
        this.image_description = image_description;
        this.title = title;
        this.description = description;
    }

    public int getImage_logo() {
        return image_logo;
    }

    public int getImage_description() {
        return image_description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setImage_logo(int image_logo) {
        this.image_logo = image_logo;
    }

    public void setImage_description(int image_description) {
        this.image_description = image_description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
