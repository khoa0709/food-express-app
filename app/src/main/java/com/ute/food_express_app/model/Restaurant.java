package com.ute.food_express_app.model;

public class Restaurant {
    private int id;
    private String name;
    private String openedTime;
    private String finishedTime;
    private String backgroundUrl;
    private String number;
    private String street;
    private int villagedId;
    private int menuCategoryId;

    public Restaurant() {

    }

    public Restaurant(int id, String name, String openedTime, String finishedTime, String backgroundUrl, String number, String street, int villagedId, int menuCategoryId) {
        this.id = id;
        this.name = name;
        this.openedTime = openedTime;
        this.finishedTime = finishedTime;
        this.backgroundUrl = backgroundUrl;
        this.number = number;
        this.street = street;
        this.villagedId = villagedId;
        this.menuCategoryId = menuCategoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenedTime() {
        return openedTime;
    }

    public void setOpenedTime(String openedTime) {
        this.openedTime = openedTime;
    }

    public String getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime(String finishedTime) {
        this.finishedTime = finishedTime;
    }

    public String getBackgroundUrl() {
        return backgroundUrl;
    }

    public void setBackgroundUrl(String backgroundUrl) {
        this.backgroundUrl = backgroundUrl;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getVillagedId() {
        return villagedId;
    }

    public void setVillagedId(int villagedId) {
        this.villagedId = villagedId;
    }

    public int getMenuCategoryId() {
        return menuCategoryId;
    }

    public void setMenuCategoryId(int menuCategoryId) {
        this.menuCategoryId = menuCategoryId;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", openedTime='" + openedTime + '\'' +
                ", finishedTime='" + finishedTime + '\'' +
                ", backgroundUrl='" + backgroundUrl + '\'' +
                ", number='" + number + '\'' +
                ", street='" + street + '\'' +
                ", villagedId=" + villagedId +
                ", menuCategoryId=" + menuCategoryId +
                '}';
    }
}
