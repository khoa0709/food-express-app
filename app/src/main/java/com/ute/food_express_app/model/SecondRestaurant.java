package com.ute.food_express_app.model;

public class SecondRestaurant {
    private int id;
    private String name;
    private float rate;
    private float distance;
    private int time;
    private int discount;
    private String address;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    private String img;

    public SecondRestaurant() {
    }

    public SecondRestaurant(int id, String name, float rate, float distance, int time) {
        this.id = id;
        this.name = name;
        this.rate = rate;
        this.distance = distance;
        this.time = time;
    }

    public SecondRestaurant(int id, String name, float rate, int time, String address) {
        this.id = id;
        this.name = name;
        this.rate = rate;
        this.time = time;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
