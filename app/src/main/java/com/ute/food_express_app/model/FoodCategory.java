package com.ute.food_express_app.model;

public class FoodCategory {
    private int id;
    private String name;
    private int background;

    public FoodCategory(int id, int background, String name) {
        this.id = id;
        this.name = name;
        this.background = background;
    }

    public FoodCategory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }
}
