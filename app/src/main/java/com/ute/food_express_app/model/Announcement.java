package com.ute.food_express_app.model;

import android.graphics.Color;
import android.graphics.drawable.Drawable;

public class Announcement {
    public int id;
    public String nameAnnoun;
    public String imageUrl;
    public String detail;
    public String createdTime;
    public boolean checked;
    public int type;
    public int customerId;


    public Announcement() {
    }

    public Announcement(int id, String nameAnnoun, String imageUrl, String detail, String createdTime, boolean checked, int type, int customerId) {
        this.id = id;
        this.nameAnnoun = nameAnnoun;
        this.imageUrl = imageUrl;
        this.detail = detail;
        this.createdTime = createdTime;
        this.checked = checked;
        this.type = type;
        this.customerId = customerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameAnnoun() {
        return nameAnnoun;
    }

    public void setNameAnnoun(String nameAnnoun) {
        this.nameAnnoun = nameAnnoun;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
