package com.ute.food_express_app.model;

public class Feedback {
    private String id;
    private String userId;
    private String username;
    private String restaurantId;
    private int rate;
    private String detail;
    private int image_review_1;
    private int image_review_2;
    private int image_avt;
    private String createTime;

    public Feedback(String id, String userId, String username, String restaurantId, int rate, String detail, int image_review_1, int image_review_2, String createTime, int image_avt) {
        this.id = id;
        this.userId = userId;
        this.username = username;
        this.restaurantId = restaurantId;
        this.rate = rate;
        this.detail = detail;
        this.image_review_1 = image_review_1;
        this.image_review_2 = image_review_2;
        this.image_avt = image_avt;
        this.createTime = createTime;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public int getRate() {
        return rate;
    }

    public String getDetail() {
        return detail;
    }

    public int getImageReview1() {
        return image_review_1;
    }

    public int getImageReview2() {
        return image_review_2;
    }

    public String getCreateTime() {
        return createTime;
    }

    public int getImage_avt() {
        return image_avt;
    }

    public void setImage_avt(int image_avt) {
        this.image_avt = image_avt;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setImageReivew1(int image_review_1) {
        this.image_review_1 = image_review_1;
    }

    public void setImageReivew2(int image_review_2) {
        this.image_review_2 = image_review_2;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
