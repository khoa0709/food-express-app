package com.ute.food_express_app.model;

import java.util.Date;

public class Feedback2 {
    private int id;
    private int rate;
    private String detail;
    private int restaurantId;
    private Date createTime;
    private Integer customerId;

    public Feedback2() {
    }

    public Feedback2(int id, int rate, String detail, int restaurantId, Date createTime, Integer customerId) {
        this.id = id;
        this.rate = rate;
        this.detail = detail;
        this.restaurantId = restaurantId;
        this.createTime = createTime;
        this.customerId = customerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
}
