package com.ute.food_express_app.model;

import java.io.Serializable;

public class Food implements Serializable {
    private Integer id;
    private String name;
    private Integer price;
    private String imageUrl;
    private Integer restaurantId;
    private Integer quantityOrder = 0;

    public Food() {

    }

    public Food(Integer id, String name, Integer price, String imageUrl, Integer restaurantId, Integer quantityOrder) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imageUrl = imageUrl;
        this.restaurantId = restaurantId;
        this.quantityOrder = quantityOrder;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getRestaurantId() {
        return restaurantId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getQuantityOrder() {
        return quantityOrder;
    }

    public void setQuantityOrder(Integer quantityOrder) {
        this.quantityOrder = quantityOrder;
    }

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", imageUrl='" + imageUrl + '\'' +
                ", restaurantId=" + restaurantId +
                ", quantityOrder=" + quantityOrder +
                '}';
    }
}
