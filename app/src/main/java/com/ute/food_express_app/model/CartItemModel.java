package com.ute.food_express_app.model;

public class CartItemModel {
    private int id;
    private String name;
    private String address;
    private String price; //Should be money
    private String number; //Should be integer
    private String payment;
    private String date;
    private String status;

    public CartItemModel(int id, String name, String address, String price, String number, String payment, String date, String status) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.price = price;
        this.number = number;
        this.payment = payment;
        this.date = date;
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }
}
