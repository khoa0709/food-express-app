package com.ute.food_express_app.model;

public class ImageSlide {
    private int res_id;

    public ImageSlide(int res_id) {
        this.res_id = res_id;
    }

    public int getRes_id() {
        return res_id;
    }

    public void setRes_id(int res_id) {
        this.res_id = res_id;
    }
}
