package com.ute.food_express_app.model;

public class FeedbackImage {
    private int id;
    private String content;
    private int feedbackId;

    public FeedbackImage() {
    }

    public FeedbackImage(int id, String content, int feedbackId) {
        this.id = id;
        this.content = content;
        this.feedbackId = feedbackId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }
}
