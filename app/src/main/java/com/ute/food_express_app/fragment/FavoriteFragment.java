package com.ute.food_express_app.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ute.food_express_app.R;
import com.google.android.material.tabs.TabLayout;
import com.ute.food_express_app.adapter.FavouriteListAdapter;

public class FavoriteFragment extends Fragment {
    private int cusId;
    public FavoriteFragment() {
        // Required empty public constructor
    }


    TabLayout tabLayout;
    ViewPager2 viewPaper;
    FavouriteListAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);


        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        viewPaper = (ViewPager2) view.findViewById(R.id.view_paper);



        FragmentManager fm = getActivity().getSupportFragmentManager();
        adapter = new FavouriteListAdapter(fm, getLifecycle());
        viewPaper.setAdapter(adapter);

        tabLayout.addTab(tabLayout.newTab().setText("Mới lưu"));
        tabLayout.addTab(tabLayout.newTab().setText("Gần tôi"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPaper.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPaper.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });


        return view;
    }
}