package com.ute.food_express_app.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Network;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.FavouriteAdapter;
import com.ute.food_express_app.adapter.MostOrderRestaurantAdapter;
import com.ute.food_express_app.adapter.NearestRestaurantAdapter;
import com.ute.food_express_app.model.Restaurant;
import com.ute.food_express_app.model.SecondRestaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class FavouriteRecentFragment extends Fragment {
    RecyclerView rcvRestaurant, rcvFavourite;
    MostOrderRestaurantAdapter restaurantAdapter;
    FavouriteAdapter favouriteAdapter;
    List<Restaurant> list;
    List<SecondRestaurant> listRestaurant;
    private int cusId;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    SharedPreferences sharedPreferences;
    int customerId;

    public FavouriteRecentFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavouriteRecent.
     */
    // TODO: Rename and change types and number of parameters
    public static FavouriteRecentFragment newInstance(String param1, String param2) {
        FavouriteRecentFragment fragment = new FavouriteRecentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            cusId = getArguments().getInt("cusId");
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        getFavouriteList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourite_recent, container, false);

        //Anh xa
        rcvRestaurant = view.findViewById(R.id.rcv_restaurant);
        rcvFavourite = view.findViewById(R.id.rcv_favourite);
        sharedPreferences = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        customerId = sharedPreferences.getInt("customerId", 0);
        //set data of hinted restaurants
        getRestaurantList();


        //set data of favourite restaurants
        rcvFavourite = view.findViewById(R.id.rcv_favourite);
        getFavouriteList();

        return view;

    }




    private void getFavouriteList() {
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/favourite/"+customerId)
            .build()
            .getAsJSONArray(new JSONArrayRequestListener() {
                @Override
                public void onResponse(JSONArray response) {
                    listRestaurant = new ArrayList<>();
                    for(int i=0;i< response.length();i++)
                    {
                        try {
                            SecondRestaurant re = new SecondRestaurant();
                            JSONObject onj = response.getJSONObject(i);
                            re.setId(onj.getInt("restaurantId"));
                            re.setImg(onj.getString("background"));
                            re.setName(onj.getString("nameRes"));
                            re.setRate(5);
                            re.setDistance(2);
                            re.setTime(17);
                            re.setAddress("");
                            listRestaurant.add(re);
                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    favouriteAdapter = new FavouriteAdapter(getActivity(), listRestaurant);
                    rcvFavourite.setAdapter(favouriteAdapter);
                    LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    rcvFavourite.setLayoutManager(horizontalLayoutManager);
                }

                @Override
                public void onError(ANError anError) {

                }
            });
    }

    private void getRestaurantList() {
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/favourite/"+customerId)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        list = new ArrayList<>();
                        for(int i=0;i< response.length();i++)
                        {
                            try {
                                Restaurant res = new Restaurant();
                                JSONObject onj = response.getJSONObject(i);
                                res.setId(onj.getInt("restaurantId"));
                                res.setBackgroundUrl(onj.getString("background"));
                                res.setName(onj.getString("nameRes"));
                                res.setFinishedTime("Đã đặt 1 ngày trước");
                                list.add(res);
                            }catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        restaurantAdapter = new MostOrderRestaurantAdapter(getContext(), list);
                        rcvRestaurant.setAdapter(restaurantAdapter);
                        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        rcvRestaurant.setLayoutManager(horizontalLayoutManager);

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
//    private List<Restaurant> getRestaurantList() {
//        List<Restaurant> list = new ArrayList<>();
//        list.add(new Restaurant(R.drawable.img_trasua, "Bông Food & Drink","Đã đặt 1 ngày trước"));
//        list.add(new Restaurant(R.drawable.img_ga, "TACAYO - Cơm gà","Đã đặt 2 ngày trước"));
//        list.add(new Restaurant(R.drawable.img_com, "Cơm trộn Hàn Quốc","Đã đặt 2 tuần trước"));
//        list.add(new Restaurant(R.drawable.img_ga, "Bông Food & Drink","Đã đặt 1 ngày trước"));
//        list.add(new Restaurant(R.drawable.img_com, "Cơm trộn Hàn Quốc","Đã đặt 2 tuần trước"));
//        return  list;
//    }
//    private List<SecondRestaurant> getFavouriteList() {
//        List<SecondRestaurant> listRestaurant = new ArrayList<>();
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_com, "Bún Măng Gà - Trưng Nữ Vương", 4, 2, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_ga, "Pizza Time - 31 Nguyễn Hoàng", 5, 1, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_trasua, "Bông Food & Drink", 4, 2, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_com, "Bún Măng Gà - Trưng Nữ Vương", 4, 2, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_ga, "Pizza Time - 31 Nguyễn Hoàng", 5, 1, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_trasua, "Bông Food & Drink", 4, 2, 17));
//
//        return  listRestaurant;
//    }
}