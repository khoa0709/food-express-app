package com.ute.food_express_app.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.FavouriteAdapter;
import com.ute.food_express_app.adapter.FavouriteListAdapter;
import com.ute.food_express_app.model.Restaurant;
import com.ute.food_express_app.model.SecondRestaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavouriteNearestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavouriteNearestFragment extends Fragment {
    RecyclerView rcvFavourite;
    FavouriteAdapter adapter;
    List<SecondRestaurant> listRestaurant;
    private int cusId;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public FavouriteNearestFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavouriteNearestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavouriteNearestFragment newInstance(String param1, String param2) {
        FavouriteNearestFragment fragment = new FavouriteNearestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourite_nearest, container, false);

        rcvFavourite = view.findViewById(R.id.rcv_favourite);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        cusId = sharedPreferences.getInt("customerId",0);
//        adapter = new FavouriteAdapter(getActivity(), getNearestList());
//        rcvFavourite.setAdapter(adapter);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        rcvFavourite.setLayoutManager(linearLayoutManager);
        getNearestList();
        return view;
    }

    private void getNearestList() {
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/favourite/"+cusId)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        listRestaurant = new ArrayList<>();
                        for(int i=0;i< response.length();i++)
                        {
                            try {
                                SecondRestaurant re = new SecondRestaurant();
                                JSONObject onj = response.getJSONObject(i);
                                re.setId(onj.getInt("restaurantId"));
                                re.setImg(onj.getString("background"));
                                re.setName(onj.getString("nameRes"));
                                re.setRate(5);
                                re.setDistance(2);
                                re.setTime(17);
                                re.setAddress("");
                                listRestaurant.add(re);
                            }catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        adapter = new FavouriteAdapter(getActivity(), listRestaurant);
                        rcvFavourite.setAdapter(adapter);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        rcvFavourite.setLayoutManager(linearLayoutManager);
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }


//    private List<SecondRestaurant> getNearestList() {
//        List<SecondRestaurant> listRestaurant = new ArrayList<>();
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_com, "Bún Măng Gà - Trưng Nữ Vương", 4, 2, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_ga, "Pizza Time - 31 Nguyễn Hoàng", 5, 1, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_trasua, "Bông Food & Drink", 4, 2, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_com, "Bún Măng Gà - Trưng Nữ Vương", 4, 2, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_ga, "Pizza Time - 31 Nguyễn Hoàng", 5, 1, 17));
//        listRestaurant.add(new SecondRestaurant(R.drawable.img_trasua, "Bông Food & Drink", 4, 2, 17));
//
//        return  listRestaurant;
//    }

}