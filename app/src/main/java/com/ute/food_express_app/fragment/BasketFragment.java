package com.ute.food_express_app.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.CartItemAdapter;
import com.ute.food_express_app.model.CartItemModel;
import com.ute.food_express_app.model.Customer;
import com.ute.food_express_app.model2.Order;
import com.ute.food_express_app.model2.OrderDetail;
import com.ute.food_express_app.model2.Restaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BasketFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BasketFragment extends Fragment {
    RecyclerView recycleView;
    CartItemAdapter adapter;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private int customerId;
    private List<Order> orders;

    public BasketFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BasketFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BasketFragment newInstance(String param1, String param2) {
        BasketFragment fragment = new BasketFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        customerId = sharedPreferences.getInt("customerId", 0);

        loadData();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_basket, container, false);

//        Set adapter for recycle view
        recycleView = (RecyclerView) view.findViewById(R.id.recycle_view);
        adapter = new CartItemAdapter(getContext(), orders);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        recycleView.setLayoutManager(gridLayoutManager);
        recycleView.setAdapter(adapter);
        return view;
    }

    private List<CartItemModel> getListCart() {
        List<CartItemModel> listCartItem = new ArrayList<>();
        listCartItem.add(new CartItemModel(R.drawable.img_com, "Bông food and drink", "132 Lê Văn Hiển", "100.000đ", "3 phần", "Tiền mặt", "11/11/2021", "hoanthanh"));
        listCartItem.add(new CartItemModel(R.drawable.img_trasua, "Toco Toco", "146 Lê Hữu Trác", "70.000đ", "2 phần", "Tiền mặt", "11/11/2021", ""));

        return listCartItem;
    }

    public void loadData(){
        orders = new ArrayList<>();

        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/order/" + customerId)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {

                        for(int i=response.length(); i>=0; i--) {
                            try {
                                JSONObject obj = response.getJSONObject(i);

                                Order order = new Order();
                                order.setId(obj.getInt("id"));
                                order.setCustomerId(customerId);
                                order.setOrderStatus(obj.getInt("orderStatus"));

                                Instant createdInstant = Instant.parse(obj.getString("createdTime"));
                                Date createdDate = Date.from(createdInstant);
                                order.setCreatedTime(createdDate);

                                orders.add(order);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        for(Order order : orders){
                            loadOrderDetail(order);
                        }

                        final Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        }, 1000);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("Duong Error: ", anError + "");
                    }
                });
    }

    public void loadOrderDetail(Order order){

        List<OrderDetail> orderDetails = new ArrayList<>();

        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/orderDetail/" + order.getId())
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {

                        for(int i=0; i<response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);

                                OrderDetail orderDetail = new OrderDetail();

                                orderDetail.setOrderId(obj.getInt("orderId"));
                                orderDetail.setFoodId(obj.getInt("foodId"));
                                orderDetail.setPrice(obj.getInt("price"));
                                orderDetail.setQuantity(obj.getInt("quantity"));

                                orderDetails.add(orderDetail);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        order.setOrderDetails(orderDetails);
                        loadRestaurantData(order);
//
//
//                        final Handler handler = new Handler(Looper.getMainLooper());
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                adapter.notifyDataSetChanged();
//                            }
//                        }, 10);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("Duong Error: ", anError + "");
                    }
                });
        }

        public void loadRestaurantData(Order order){
            Log.v("Duong order i: ", order.toString());
            int idd = order.getOrderDetails().get(0).getFoodId();

            AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/food/id/" + idd)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void onResponse(JSONArray response) {

                            try {
                                JSONObject object = response.getJSONObject(0);

                                Log.v("Duonng 111111111111: ", object.toString());
                                Restaurant restaurant = new Restaurant();
                                restaurant.setId(object.getInt("restaurantId"));
                                order.setRestaurant(restaurant);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/restaurant/id/" + order.getRestaurant().getId())
                                    .build()
                                    .getAsJSONArray(new JSONArrayRequestListener() {
                                        @RequiresApi(api = Build.VERSION_CODES.O)
                                        @Override
                                        public void onResponse(JSONArray response) {
                                            try {
                                                JSONObject obj = response.getJSONObject(0);
                                                order.getRestaurant().setId(obj.getInt("id"));
                                                order.getRestaurant().setName(obj.getString("nameRes"));
                                                order.getRestaurant().setBackground(obj.getString("background"));
                                                order.getRestaurant().setNumber(obj.getInt("number"));
                                                order.getRestaurant().setStreet(obj.getString("street"));

                                                adapter.notifyDataSetChanged();

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }

                                        @Override
                                        public void onError(ANError anError) {
                                            Log.v("Error Duongggggggggg", anError.toString());
                                        }
                                    });
                        }
                        @Override
                        public void onError(ANError anError) {
                            Log.v("Error Duongggggggggg", anError.toString());
                        }
                    });
//                        Log.v("Duong order detail", order.toString());
        }
}