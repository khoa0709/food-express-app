package com.ute.food_express_app.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ute.food_express_app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Register_tab_fragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    public Register_tab_fragment() {
        // Required empty public constructor
    }

    public static Register_tab_fragment newInstance(String param1, String param2) {
        Register_tab_fragment fragment = new Register_tab_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    Button btnRegister;
    TextView txtLogin;
    EditText txtName, txtPhone, txtPass, txtPassCf;
    private String name, phone, pass, passcf, url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_tab, container, false);
        txtLogin = (TextView) view.findViewById(R.id.register_login);
        txtName = view.findViewById(R.id.register_name);
        txtPhone = view.findViewById(R.id.register_phone);
        txtPass = view.findViewById(R.id.register_password);
        txtPassCf = view.findViewById(R.id.register_password2);
        btnRegister = view.findViewById(R.id.register_button);
        url = "http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/createCustomer";

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = txtName.getText().toString().trim();
                phone = txtPhone.getText().toString().trim();
                pass = txtPass.getText().toString().trim();
                passcf = txtPassCf.getText().toString().trim();
                Boolean validation = validate(name, phone, pass, passcf);
                if(validation) {
                    register();
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ViewPager2 viewPager2 = (ViewPager2) getActivity().findViewById(R.id.auth_views);
                            viewPager2.setCurrentItem(0);
                        }
                    }, 2000);
                }
            }
        });

        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewPager2 viewPager2 = (ViewPager2) getActivity().findViewById(R.id.auth_views);
                viewPager2.setCurrentItem(0);
            }
        });
        return view;
    }

    private void register() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("phone", phone);
            jsonObject.put("fullName", name);
            jsonObject.put("pass", pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/customer/createCustomer")
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(getContext(), response.getString("status"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private Boolean validate(String name, String phone, String pass, String passcf) {
        if(name.length()==0) {
            txtName.requestFocus();
            txtName.setError("Tên không được trống");
            return false;
        } else if(phone.length()==0) {
            txtPhone.requestFocus();
            txtPhone.setError("Số điện thoại không được trống");
            return false;
        } else if(!phone.matches("^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$")) {
            txtPhone.requestFocus();
            txtPhone.setError("Số điện thoại không hợp lệ");
            return false;
        } else if(pass.length() < 5) {
            txtPass.requestFocus();
            txtPass.setError("Mật khẩu bắt buộc trên 5 kí tự");
            return false;
        } else if(!passcf.equalsIgnoreCase(pass)) {
            txtPassCf.requestFocus();
            txtPassCf.setError("Mật khẩu nhập lại không khớp");
            return false;
        }
        return true;
    }
}