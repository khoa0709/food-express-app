package com.ute.food_express_app.fragment;


import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.NotificationAdapter;
import com.ute.food_express_app.model2.Notification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

public class NotificationFragment extends Fragment {
    private String mParam1;
    private String mParam2;
    private ListView lvContact;
    ArrayList<Notification> list;
    private int cusId;
    NotificationAdapter adapter;
    RecyclerView rcvNoti;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        rcvNoti = view.findViewById(R.id.rcv_noti);
        getNotiList();
        return view;
    }
    private void getNotiList() {
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/announcement")
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        list = new ArrayList<>();
                        for(int i=0;i< response.length();i++)
                        {
                            try {
                                Notification notification = new Notification();
                                JSONObject onj = response.getJSONObject(i);
                                notification.setId(onj.getInt("id"));
                                notification.setTitle(onj.getString("title"));
                                notification.setImageUrl(onj.getString("imageUrl"));
                                notification.setContent(onj.getString("content"));
                                Instant fm = Instant.parse(onj.getString("createdTime"));
                                Date d = Date.from(fm);
                                SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
                                String e = simpleDate.format(d);
                                notification.setCreatedTime(e);
                                list.add(notification);
                            }catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        NotificationAdapter customAdaper = new NotificationAdapter(getActivity(),list);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        rcvNoti.setLayoutManager(linearLayoutManager);
                        rcvNoti.setAdapter(customAdaper);
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}