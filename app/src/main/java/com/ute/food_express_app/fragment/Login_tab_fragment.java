package com.ute.food_express_app.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.ute.food_express_app.actvitie.YourLocation1Activity;
import com.ute.food_express_app.model.Customer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Login_tab_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class Login_tab_fragment extends Fragment {
    TextView txtForgot;
    EditText txtPhone, txtPassWord;
    Button btnLogin;
    Customer customerLogin;
    List<Customer> listCustomer;
    SharedPreferences sharedPreferences;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Login_tab_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Login_tab_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Login_tab_fragment newInstance(String param1, String param2) {
        Login_tab_fragment fragment = new Login_tab_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_tab, container, false);
        txtForgot = (TextView) view.findViewById(R.id.login_forgot);
        txtPhone = (EditText) view.findViewById(R.id.login_phone);
        txtPassWord = (EditText) view.findViewById(R.id.login_password);
        btnLogin = (Button) view.findViewById(R.id.login_button);

        customerLogin = null;
        listCustomer = new ArrayList<>();
        sharedPreferences = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        // Lay tk mk da dang nhap
        txtPhone.setText(sharedPreferences.getString("taikhoan", ""));
        txtPassWord.setText(sharedPreferences.getString("matkhau", ""));

        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewPager2 viewPager2 = (ViewPager2) getActivity().findViewById(R.id.auth_views);
                viewPager2.setCurrentItem(2);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = txtPhone.getText().toString().trim();
                String pass = txtPassWord.getText().toString().trim();

                if (phone.equalsIgnoreCase("") || pass.equalsIgnoreCase("")) {

                } else {
                    loadData();
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Boolean loginResult = checkUser();
                            if (loginResult) {
                                // In thong bao
                                Toast.makeText(getActivity(), "Đăng nhập thành công", Toast.LENGTH_SHORT).show();

                                // Luu tk mk
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("taikhoan", phone);
                                editor.putString("matkhau", pass);
                                editor.putInt("customerId", customerLogin.getId());
                                editor.commit();

                                // Vao trang chu
                                Intent intent = new Intent(getActivity(), YourLocation1Activity.class);
                                intent.putExtra("id", customerLogin.getId());
                                startActivity(intent);
                            } else {
                                Toast.makeText(getActivity(), "Tài khoản hoặc mật khẩu không đúng", Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }
                    }, 2000);
                }
            }
        });
        return view;
    }

    private boolean checkUser() {
        String phoneNumber = txtPhone.getText().toString().trim();
        String password = txtPassWord.getText().toString().trim();
        if (listCustomer == null || listCustomer.isEmpty())
            return false;
        for (Customer customer : listCustomer) {
            if (phoneNumber.equals(customer.getPhone()) && password.equals(customer.getPass())) {
                customerLogin = customer;
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("customerId", customerLogin.getId());
                editor.commit();
                return true;
            }
        }

        return false;
    }

    private void loadData() {
        //
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/customer")
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Customer customer = new Customer();
                                customer.setId(obj.getInt("id"));
                                customer.setFullName(obj.getString("fullName"));
                                customer.setEmail(obj.getString("email"));
                                customer.setPhone(obj.getString("phone"));
                                customer.setPass(obj.getString("pass"));
                                listCustomer.add(customer);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("Error: ", anError + "");
                    }
                });
    }

}