package com.ute.food_express_app.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ute.food_express_app.R;
import com.ute.food_express_app.actvitie.OrderActivity;
import com.ute.food_express_app.adapter.CartFoodAdapter;
import com.ute.food_express_app.model.MyFood;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment {

    private Button btnDelivery;
    List<MyFood> foodList;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public CartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CartFragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_cart, container, false);

        btnDelivery = (Button) view.findViewById(R.id.btnDelivery);

        //cart food
        RecyclerView rcvCartFood = view.findViewById(R.id.rcvCartFood);
        List<MyFood> foodList = getFoodList();
//        CartFoodAdapter cartFoodAdapter = new CartFoodAdapter(view.getContext(), foodList);
//        rcvCartFood.setAdapter(cartFoodAdapter);
//        LinearLayoutManager vertical = new LinearLayoutManager(CartFragment.this, LinearLayoutManager.VERTICAL, false);
        rcvCartFood.setLayoutManager(new LinearLayoutManager(view.getContext()));

        btnDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
//                bundle.putSerializable("foodList", foodList);

                Intent intent = new Intent(view.getContext(), OrderActivity.class);
                intent.putExtra("value", bundle);
                startActivity(intent);
            }
        });

        return view;
    }

    public  List<MyFood> getFoodList(){
        foodList = new ArrayList<>();

        foodList.add(new MyFood(R.drawable.img_food_7, "Soda dâu", "Bông Food & Drink", "20.000", 1));
        foodList.add(new MyFood(R.drawable.img_food_8, "Sữa chua việt quất", "Bông Food & Drink", "20.000", 2));
        foodList.add(new MyFood(R.drawable.img_food_9, "Trà kiwi nha đam", "Bông Food & Drink", "20.000", 1));

        return foodList;
    }
}