package com.ute.food_express_app.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;


import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.actvitie.RestaurantDetailActivity;
import com.ute.food_express_app.adapter.CartFoodAdapter;
import com.ute.food_express_app.adapter.ListFoodAdapter;
import com.ute.food_express_app.model.Food;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Set_Menu_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Set_Menu_Fragment extends Fragment {

    Context context;
    ConstraintLayout layoutCart;
    ConstraintLayout layoutCartTemporary;
//    RecyclerView rcv_listFoodCartTemp;
//    CartFoodAdapter cartFoodAdapter;
//    ArrayList<Food> listFoodTemp;

    View v;
    RecyclerView rcv_food;
    ArrayList<Food> listFood = new ArrayList<>();
    ListFoodAdapter listFoodAdapter;

    RestaurantDetailActivity restaurantDetailActivity;



    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int restaurantId;

    public Set_Menu_Fragment() {
    }

    public Set_Menu_Fragment(Context context, ConstraintLayout layoutCart, ConstraintLayout layoutCartTemporary) {
        this.context = context;
        this.layoutCart = layoutCart;
        this.layoutCartTemporary = layoutCartTemporary;
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Set_Menu_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Set_Menu_Fragment newInstance(String param1, String param2) {
        Set_Menu_Fragment fragment = new Set_Menu_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_set_menu_, container, false);



        restaurantDetailActivity = (RestaurantDetailActivity) getActivity();
        rcv_food = (RecyclerView) v.findViewById(R.id.rcv_list_food);
        getListFood(listFood);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                listFoodAdapter = new ListFoodAdapter(context, listFood, layoutCart, layoutCartTemporary);
                rcv_food.setLayoutManager(linearLayoutManager);
                rcv_food.setAdapter(listFoodAdapter);
            }
        }, 2000);



        return v;
    }

    public void getListFood(ArrayList<Food> listFood) {

        int id = restaurantDetailActivity.getRestaurantId();
        Log.v("1. Duonng restaurant Id", String.valueOf(id));
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/food/" + id)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                Food food = new Food();

                                JSONObject object = response.getJSONObject(i);

                                food.setId(object.getInt("id"));
                                food.setName(object.getString("nameFood"));
                                food.setImageUrl(object.getString("imageFood"));
                                food.setPrice(object.getInt("price"));
                                food.setRestaurantId(object.getInt("restaurantId"));

                                listFood.add(food);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.v("Error Duongggggggggg", anError.toString());
                    }
                });
    }

}