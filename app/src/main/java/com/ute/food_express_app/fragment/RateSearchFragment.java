package com.ute.food_express_app.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.SearchItemAdapter;
import com.ute.food_express_app.model.Restaurant;
import com.ute.food_express_app.model.SecondRestaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RateSearchFragment extends Fragment {

    RecyclerView recycleView;
    SearchItemAdapter adapter;
    String id;


    // TODO: Rename and change types of parameters
    public RateSearchFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }
    public RateSearchFragment(String id) {
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_rate_search, container, false);
        recycleView = (RecyclerView) view.findViewById(R.id.recycle_view);
        adapter = new SearchItemAdapter(getContext(), getListRestaurant(id));
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
                recycleView.setLayoutManager(gridLayoutManager);
                recycleView.setAdapter(adapter);
            }
        }, 2000);

        return view;
    }

    public List<Restaurant> getListRestaurant(String id) {

        List<Restaurant> listRestaurant = new ArrayList<>();
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/restaurant/menuId/{id}")
                .addPathParameter("id", id)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                Restaurant restaurant = new Restaurant();
                                JSONObject obj = response.getJSONObject(i);
                                restaurant.setId(obj.getInt("id"));
                                restaurant.setName(obj.getString("nameRes"));
                                restaurant.setOpenedTime(obj.getString("openedTime"));
                                restaurant.setFinishedTime(obj.getString("finishedTime"));
                                restaurant.setBackgroundUrl(obj.getString("background"));
                                restaurant.setNumber(obj.getString("number"));
                                restaurant.setStreet(obj.getString("street"));
                                restaurant.setVillagedId(obj.getInt("villagedId"));
                                restaurant.setMenuCategoryId(obj.getInt("menuCategoryId"));

                                listRestaurant.add(restaurant);

                            } catch (JSONException e) {
                                // e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

        return  listRestaurant;
    }



}