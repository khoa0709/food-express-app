package com.ute.food_express_app.fragment;

import static java.lang.String.valueOf;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.actvitie.CollectionAtivity;
import com.ute.food_express_app.actvitie.ListSearchResActivity;
import com.ute.food_express_app.actvitie.LocationActivity;
import com.ute.food_express_app.actvitie.MainActivity2;
import com.ute.food_express_app.actvitie.SearchActivity;
import com.ute.food_express_app.adapter.FoodCategoryAdapter;
import com.ute.food_express_app.adapter.ImageSlideAdapter;
import com.ute.food_express_app.adapter.ListRestaurantAdapter;
import com.ute.food_express_app.adapter.MostOrderRestaurantAdapter;
import com.ute.food_express_app.adapter.NearestRestaurantAdapter;
import com.ute.food_express_app.model.FoodCategory;
import com.ute.food_express_app.model.ImageSlide;
import com.ute.food_express_app.model.Restaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class HomeFragment extends Fragment {
    private String data;
    //private ImageView searchTab;
    private TextView txtAllCollections;
    private TextView txtAddress;
    private ViewPager viewPager;
    private CircleIndicator circleIndicator;
    private ImageSlideAdapter imageSlide_adapter;
    private List<ImageSlide> imageSlideList;
    private Timer timer;
    public LocationActivity mlocationActivity;
    public MainActivity2 mainActivity2;
    public ImageView btnChicken;
    private RecyclerView rcvRestaurant, rcvSecondRestaurant;
    private String result;
    private SearchView searchTab;

    List<Restaurant> mostOrderRestaurantList = new ArrayList<>();
    RecyclerView rcvListRestaurant;
    ArrayList<Restaurant> listRestaurant = new ArrayList<>();
    ListRestaurantAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    ImageButton btn_bakery, btn_chicken, btn_drink, btn_fastFood, btn_beer, btn_pizza, btn_cream, btn_noodle;

    public HomeFragment() {
        // Required empty public constructor
    }
    public HomeFragment(String data) {
        this.data = data;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        btn_bakery = v.findViewById(R.id.btn_bakery);
        btn_chicken = v.findViewById(R.id.btn_chicken);
        btn_drink = v.findViewById(R.id.btn_drink);
        btn_fastFood = v.findViewById(R.id.btn_fastFood);
        btn_beer = v.findViewById(R.id.btn_beer);
        btn_pizza = v.findViewById(R.id.btn_pizza);
        btn_cream = v.findViewById(R.id.btn_cream);
        btn_noodle = v.findViewById(R.id.btn_noodle);

        initSearchByMenuCategory();



        // Search
        searchTab = v.findViewById(R.id.edt_search);
        searchTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
            }
        });

        // Current Location
        SharedPreferences myPref = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        String address = myPref.getString("address", "...");
        txtAddress = v.findViewById(R.id.txtAddress);
        txtAddress.setText(address);

        // Collections
        txtAllCollections = v.findViewById(R.id.all_collection);
        txtAllCollections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_Collections = new Intent(getActivity(), CollectionAtivity.class);
                startActivity(intent_Collections);
            }
        });

        // Image Slide
        viewPager = v.findViewById(R.id.view_pager);
        circleIndicator = v.findViewById(R.id.circle_indicator);

        imageSlideList = getListImage();
        imageSlide_adapter = new ImageSlideAdapter(getActivity(), imageSlideList);
        viewPager.setAdapter(imageSlide_adapter);

        circleIndicator.setViewPager(viewPager);
        imageSlide_adapter.registerDataSetObserver(circleIndicator.getDataSetObserver());

        autoSlideImage();

        // Most order Restaurant list
        rcvRestaurant = v.findViewById(R.id.rcv_restaurant);
        mostOrderRestaurantList = getListMostOrderRestaurant();
        MostOrderRestaurantAdapter mostOrderRestaurantAdapter = new MostOrderRestaurantAdapter(getActivity(),
                mostOrderRestaurantList);
        rcvRestaurant.setAdapter(mostOrderRestaurantAdapter);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        rcvRestaurant.setLayoutManager(horizontalLayoutManagaer);

        // nearest restaurant list
        rcvSecondRestaurant = v.findViewById(R.id.rcvSecondRestaurant);
        List<Restaurant> nearestRestaurantList = getSecondRestaurantList();
        NearestRestaurantAdapter nearestRestaurantAdapter = new NearestRestaurantAdapter(getActivity(),
                nearestRestaurantList);
        rcvSecondRestaurant.setAdapter(nearestRestaurantAdapter);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        rcvSecondRestaurant.setLayoutManager(horizontalLayoutManager);



        // restaurant list
        rcvListRestaurant = v.findViewById(R.id.rcvListRestaurant);
        getData(listRestaurant);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter = new ListRestaurantAdapter(getContext(), listRestaurant);
                linearLayoutManager = new LinearLayoutManager(getContext());
                rcvListRestaurant.setLayoutManager(linearLayoutManager);
                rcvListRestaurant.setAdapter(adapter);
            }
        }, 2000);

        // food category list
        final Handler handler1 = new Handler(Looper.getMainLooper());
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                RecyclerView rcvFoodCategory = v.findViewById(R.id.rcvFoodCategory);
                List<FoodCategory> foodCategoryList = getFoodCategoryList();
                FoodCategoryAdapter foodCategoryAdapter = new FoodCategoryAdapter(getActivity(), foodCategoryList, adapter, listRestaurant);
                rcvFoodCategory.setAdapter(foodCategoryAdapter);
                LinearLayoutManager horizontalLayoutManager1 = new LinearLayoutManager(getActivity(),
                        LinearLayoutManager.HORIZONTAL, false);
                rcvFoodCategory.setLayoutManager(horizontalLayoutManager1);
            }
        }, 3000);
        return v;
    }

    private List<ImageSlide> getListImage() {
        List<ImageSlide> list = new ArrayList<>();
        list.add(new ImageSlide(R.drawable.img_tabslide1));
        list.add(new ImageSlide(R.drawable.img_tabslide2));
        list.add(new ImageSlide(R.drawable.img_tabslide3));
        list.add(new ImageSlide(R.drawable.img_tabslide4));

        return list;
    }

    private void autoSlideImage() {
        if (imageSlideList == null || imageSlideList.isEmpty() || viewPager == null) {
            return;
        }

        // Init timer
        if (timer == null) {
            timer = new Timer();
        }

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        int currentItem = viewPager.getCurrentItem();
                        int totalItem = imageSlideList.size() - 1;
                        if (currentItem < totalItem) {
                            currentItem++;
                            viewPager.setCurrentItem(currentItem);
                        } else {
                            viewPager.setCurrentItem(0);
                        }
                    }
                });
            }
        }, 1000, 3000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private List<Restaurant> getListMostOrderRestaurant() {
        List<Restaurant> list = new ArrayList<>();
        list.add(new Restaurant(1, "Pizza Pasta & Fast Food", "", "", "https://i.ibb.co/93ZVtth/pizza-pasta-food.jpg", "", "", 1,
                1));
        list.add(new Restaurant(2, "Nhà hàng Pizza Hut", "", "", "https://i.ibb.co/BKZmqwc/pizza-hut.jpg", "", "", 1,
                1));
        list.add(new Restaurant(7, "Tiramisu Mr Huân", "", "", "https://i.ibb.co/GHJgZhr/cake-mrhuan.jpg", "", "",
                1, 1));

        return list;
    }

    private List<Restaurant> getSecondRestaurantList() {
        List<Restaurant> list = new ArrayList<>();

        list.add(new Restaurant(6, "Tiệm bánh Handmade Mẹ Bắp", "", "", "https://i.ibb.co/p6TkqrW/cake-xuka.jpg", "", "", 1,
                1));
        list.add(new Restaurant(13, "Gà nướng Linh Lan", "", "", "https://i.ibb.co/17kyZNY/chicken-rice.jpg", "", "", 1,
                1));
        list.add(new Restaurant(17, "Kem Bơ Cô Điếc", "", "", "https://i.ibb.co/mGgt1rT/cream-flan.jpg", "", "",
                1, 1));

        return list;
    }

    private List<FoodCategory> getFoodCategoryList() {
        List<FoodCategory> foodCategoryList = new ArrayList<>();

        foodCategoryList.add(new FoodCategory(10, R.drawable.icon_vegetarian, "Tất cả"));
        foodCategoryList.add(new FoodCategory(2, R.drawable.icon_food, "Đồ ăn"));
        foodCategoryList.add(new FoodCategory(4, R.drawable.icon_drink, "Đồ uống"));
        foodCategoryList.add(new FoodCategory(1, R.drawable.icon_sweet_cake, "Bánh ngọt"));
        foodCategoryList.add(new FoodCategory(5, R.drawable.icon_snack, "Ăn Vặt"));
        foodCategoryList.add(new FoodCategory(8, R.drawable.img_donut, "Đồ chay"));

        return foodCategoryList;
    }

    public void getData(ArrayList<Restaurant> listRestaurant) {

        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/restaurant/")
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                Restaurant restaurant = new Restaurant();
                                JSONObject obj = response.getJSONObject(i);
                                restaurant.setId(obj.getInt("id"));
                                restaurant.setName(obj.getString("nameRes"));
                                restaurant.setOpenedTime(obj.getString("openedTime"));
                                restaurant.setFinishedTime(obj.getString("finishedTime"));
                                restaurant.setBackgroundUrl(obj.getString("background"));
                                restaurant.setNumber(obj.getString("number"));
                                restaurant.setStreet(obj.getString("street"));
                                restaurant.setVillagedId(obj.getInt("villagedId"));
                                restaurant.setMenuCategoryId(obj.getInt("menuCategoryId"));

                                listRestaurant.add(restaurant);

                            } catch (JSONException e) {
                                // e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    private void initSearchByMenuCategory() {
        btn_bakery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListSearchResActivity.class);
                intent.putExtra("id", "1");
                intent.putExtra("name", "Bánh ngọt");
                startActivity(intent);
            }
        });

        btn_chicken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListSearchResActivity.class);
                intent.putExtra("id", "2");
                intent.putExtra("name", "Các món gà");
                startActivity(intent);
            }
        });

        btn_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListSearchResActivity.class);
                intent.putExtra("id", "4");
                intent.putExtra("name", "Đồ uống");
                startActivity(intent);
            }
        });

        btn_cream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListSearchResActivity.class);
                intent.putExtra("id", "3");
                intent.putExtra("name", "Các món kem");
                startActivity(intent);
            }
        });

        btn_beer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListSearchResActivity.class);
                intent.putExtra("id", "9");
                intent.putExtra("name", "Bia");
                startActivity(intent);
            }
        });

        btn_fastFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListSearchResActivity.class);
                intent.putExtra("id", "5");
                intent.putExtra("name", "Đồ ăn nhanh");
                startActivity(intent);
            }
        });

        btn_pizza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListSearchResActivity.class);
                intent.putExtra("id", "7");
                intent.putExtra("name", "Pizza");
                startActivity(intent);
            }
        });

        btn_noodle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListSearchResActivity.class);
                intent.putExtra("id", "8");
                intent.putExtra("name", "Các món mì");
                startActivity(intent);
            }
        });
    }

}