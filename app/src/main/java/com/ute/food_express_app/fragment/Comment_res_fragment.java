package com.ute.food_express_app.fragment;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.actvitie.RestaurantDetailActivity;
import com.ute.food_express_app.adapter.ListFeedbackAdapter;
import com.ute.food_express_app.model2.Feedback;
import com.ute.food_express_app.model2.FeedbackImage;
import com.ute.food_express_app.model2.Food;
import com.ute.food_express_app.model2.User;
import com.ute.food_express_app.model2.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Comment_res_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Comment_res_fragment extends Fragment {

    View v;
    RecyclerView rcv_feedback;
    ArrayList<Feedback> listFeedback;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ListFeedbackAdapter listFeedbackAdapter;
    RestaurantDetailActivity restaurantDetailActivity;

    public Comment_res_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Comment_res_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Comment_res_fragment newInstance(String param1, String param2) {
        Comment_res_fragment fragment = new Comment_res_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        listFeedback = new ArrayList<>();
//        listFeedback.add(new Feedback("1", "USER1", "Hoa", "RES1", 5, "Giao hàng nhanh, đúng sản phẩm, ngon", R.drawable.img_food_review_7, R.drawable.img_food_review_8, "11:50 1/11/2021", R.drawable.img_ava_1));
//        listFeedback.add(new Feedback("2", "USER2", "Van", "RES1", 5, "Hơi ngọt", R.drawable.img_food_review_9, R.drawable.img_food_review_4, "11:15 31/10/2021", R.drawable.img_ava_2));
//        listFeedback.add(new Feedback("3", "USER3", "Hang", "RES1", 5, "Thức uống ngon, pha vừa miệng", R.drawable.img_food_review_5, R.drawable.img_food_review_6, "17:00 30/10/2021", R.drawable.img_ava_3));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        restaurantDetailActivity = (RestaurantDetailActivity) getActivity();
        loadData();

        v = inflater.inflate(R.layout.fragment_comment_res, container, false);
        rcv_feedback = (RecyclerView) v.findViewById(R.id.rcv_list_feedback);
        listFeedbackAdapter = new ListFeedbackAdapter(getContext(), listFeedback);
        rcv_feedback.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcv_feedback.setAdapter(listFeedbackAdapter);
        return v;
    }

    public void loadData(){
        int id = restaurantDetailActivity.getRestaurantId();
        listFeedback = new ArrayList<>();
        int userId = 0;
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/feedback/" + id)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                Feedback feedback = new Feedback();

                                JSONObject object = response.getJSONObject(i);

                                feedback.setId(Integer.parseInt(object.getString("id")));
                                feedback.setDetail(object.getString("detail"));
                                feedback.setRate(object.getInt("rate"));
                                feedback.setRestaurantId(object.getInt("restaurantId"));

                                int id = object.getInt("customerId");

//                                final Handler handler = new Handler(Looper.getMainLooper());
//                                handler.postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        feedback.setUser(getUserById(id));
//                                    }
//                                }, 3000);
                                Instant createdInstant = Instant.parse(object.getString("createdTime"));
                                Date createdTime = Date.from(createdInstant);
                                feedback.setCreateTime(createdTime);

                                AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/customer/" + id)
                                        .build()
                                        .getAsJSONArray(new JSONArrayRequestListener() {

                                            @RequiresApi(api = Build.VERSION_CODES.O)
                                            @Override
                                            public void onResponse(JSONArray response) {
                                                try {
                                                    JSONObject object = response.getJSONObject(0);
                                                    User user = new User();
                                                    user.setId(id);
                                                    user.setName(object.getString("email"));
//                                                    user.setAvatar("https://i.ibb.co/GHJgZhr/cake-mrhuan.jpg");
                                                    user.setAvatar(object.getString("avatar"));

                                                    feedback.setUser(user);

                                                    AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/feedbackImage/" + feedback.getId())
                                                            .build()
                                                            .getAsJSONArray(new JSONArrayRequestListener() {
                                                                List<FeedbackImage> images = new ArrayList<>();

                                                                @RequiresApi(api = Build.VERSION_CODES.O)
                                                                @Override
                                                                public void onResponse(JSONArray response) {
                                                                    try {
                                                                        for(int i=0; i<response.length(); i++){
                                                                            JSONObject object = response.getJSONObject(i);
                                                                            FeedbackImage image = new FeedbackImage();

                                                                            image.setId(object.getInt("id"));
                                                                            image.setContent(object.getString("content"));
                                                                            image.setFeedbackId(object.getInt("feedbackId"));

                                                                            images.add(image);
                                                                        }

                                                                        feedback.setImages(images);

                                                                        listFeedback.add(feedback);

                                                                        listFeedbackAdapter.notifyDataSetChanged();

                                                                    }catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                                @Override
                                                                public void onError(ANError anError) {
                                                                    Log.v("Error Duongggggggggg", anError.toString());
                                                                }
                                                            });
                                                }catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            @Override
                                            public void onError(ANError anError) {
                                                Log.v("Error Duongggggggggg", anError.toString());
                                            }
                                        });

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.v("Error Duongggggggggg", anError.toString());
                    }
                });
    }

}

