package com.ute.food_express_app.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.actvitie.*;
import com.ute.food_express_app.adapter.MostOrderRestaurantAdapter;
import com.ute.food_express_app.model.Customer;
import com.ute.food_express_app.model.Restaurant;
import com.ute.food_express_app.model.SecondRestaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ImageButton btn_edit;
    private ConstraintLayout la;

    private Button btn_logout;
    private Customer cusLogin;
    private ConstraintLayout layout;

    //Customer
    private ImageView imgLogo;
    private TextView txtCustomerName;
    private TextView txtDob;
    private TextView txtEmail;
    private TextView txtPhone;
    private TextView txtGender;
    private TextView txtCountFavout;
    private TextView txtCountNoti;
    private int cusId;
    private int countFa=0;
    private int countNo=0;
    String gen="";
    String date="";

    private String mParam1;
    private String mParam2;

    public UserFragment() {
        // Required empty public constructor
    }
    public static UserFragment newInstance(String param1, String param2) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_user, container, false);

        btn_edit = (ImageButton) view.findViewById(R.id.edit);

        btn_logout = (Button) view.findViewById(R.id.logout);

        //Get customer Id
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        cusId = sharedPreferences.getInt("customerId",0);

        //customer find elements
        imgLogo = view.findViewById(R.id.imgLogo);
        txtCustomerName = view.findViewById(R.id.cus_name);
        txtDob = view.findViewById(R.id.cus_dob);
        txtEmail = view.findViewById(R.id.cus_email);
        txtPhone = view.findViewById((R.id.cus_phone));
        txtGender = view.findViewById((R.id.cus_gender));
        txtCountFavout = view.findViewById((R.id.count_favout));
        txtCountNoti = view.findViewById((R.id.count_noti));

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UserInforActivity.class);
                intent.putExtra("user", (Serializable) cusLogin);
                startActivity(intent);
            }
        });


        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("taikhoan", "");
                editor.putString("matkhau", "");
                editor.commit();
                Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
                startActivity(intent);
            }
        });

        // Inflate the layout for this fragment
        cusLogin = new Customer();
        getUser(cusLogin);


        return view;
    }


    private void getUser(Customer cus) {
        countFa =0;
        countNo = 0;
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/announcement")
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i< response.length();i++)
                        {
                            countNo = countNo +1;
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/favourite/"+cusId)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i< response.length();i++)
                        {
                            countFa = countFa +1;
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/customer/"+cusId)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i< response.length();i++)
                        {
                            try {

                                JSONObject onj = response.getJSONObject(i);
                                cusLogin.setId(onj.getInt("id"));
                                cusLogin.setFullName(onj.getString("fullName"));
                                cusLogin.setPhone(onj.getString("phone"));
                                cusLogin.setPass(onj.getString("pass"));

                                if(onj.getString("email") != null && !onj.getString("email").isEmpty() && !onj.getString("email").equals("null")){
                                    cusLogin.setEmail(onj.getString("email"));
                                }
                                else
                                    cusLogin.setEmail("Chưa có");

                                cusLogin.setGender(onj.getString("gender"));
                                if(cusLogin.getGender().isEmpty())
                                    cusLogin.setGender("M");
                                if(cusLogin.getGender().equals("M"))
                                    gen = "Nam";
                                else if (cusLogin.getGender().equals("F"))
                                    gen = "Nữ";
                                else
                                    gen = "Chưa có";


                                if(onj.getString("avatar") != null && !onj.getString("avatar").isEmpty() && !onj.getString("avatar").equals("null")){
                                    cusLogin.setAvatar(onj.getString("avatar"));
                                }
                                else
                                    cusLogin.setAvatar("https://i.ibb.co/PNwcWF0/avatar.jpg");


                                cusLogin.setNumber(onj.getInt("number"));
                                cusLogin.setStreet(onj.getString("street"));

                                if(onj.getString("dob") != null && !onj.getString("dob").isEmpty() && !onj.getString("dob").equals("null")){
                                    Instant fm = Instant.parse(onj.getString("dob"));
                                    Date d = Date.from(fm);
                                    SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
                                    String e = simpleDate.format(d);
                                    cusLogin.setDob(e);
                                }
                                else
                                {
                                    cusLogin.setDob("Chưa có");
                                }

                            }catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }


                        if (TextUtils.isEmpty(cusLogin.getFullName()))
                            cusLogin.setDob("Chưa có");
                        if (TextUtils.isEmpty(cusLogin.getDob()))
                            cusLogin.setDob("Chưa có");
                        if(TextUtils.isEmpty(cusLogin.getEmail()))
                            cusLogin.setEmail("Chưa có");


                        Picasso.get().load(cusLogin.getAvatar()).into(imgLogo);
                        txtCustomerName.setText(cusLogin.getFullName());
                        txtDob.setText(String.valueOf(cusLogin.getDob()));
                        txtEmail.setText(String.valueOf(cusLogin.getEmail()));
                        txtPhone.setText(String.valueOf(cusLogin.getPhone()));
                        txtGender.setText(gen);
                        txtCountFavout.setText(String.valueOf(countFa));
                        txtCountNoti.setText(String.valueOf(countNo));
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("id",cusId);
        super.onSaveInstanceState(outState);
    }

}