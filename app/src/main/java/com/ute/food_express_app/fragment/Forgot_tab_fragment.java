package com.ute.food_express_app.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.model.Customer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Forgot_tab_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Forgot_tab_fragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Forgot_tab_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Forgot_tab_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Forgot_tab_fragment newInstance(String param1, String param2) {
        Forgot_tab_fragment fragment = new Forgot_tab_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    EditText txtEmail;
    Button btnSend;
    String password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_forgot_tab, container, false);
        txtEmail = view.findViewById(R.id.txtEmail);
        btnSend = view.findViewById(R.id.login_button);
        password = "";
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = txtEmail.getText().toString().trim();
                if(email.isEmpty() || !email.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")) {
                    txtEmail.requestFocus();
                    txtEmail.setError("Email không hợp lệ");
                } else {
                    findPassByEmail(email);
                    final Handler handler =  new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(!password.isEmpty()) {
                                Toast.makeText(getContext(), "Vui lòng vào hộp thư của bạn để kiểm tra!", Toast.LENGTH_SHORT).show();
                                sendEmail();
                                ViewPager2 viewPager2 = (ViewPager2) getActivity().findViewById(R.id.auth_views);
                                viewPager2.setCurrentItem(0);
                            } else {
                                Toast.makeText(getContext(), "Không tìm thấy email đăng ký", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 2000);

                }
            }
        });
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        return view;
    }

    private void findPassByEmail(String email) {
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/customer")
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for(int i=0; i<response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                if(email.equals(obj.getString("email"))) {
                                    password = obj.getString("pass");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("Error: ", anError + "");
                    }
                });
    }
    private void sendEmail() {
//      Dang nhap email gui
        final String nameSender = "foodexpressapps1@gmail.com";
        final String passSender = "abcd_1234";
        String receiver = txtEmail.getText().toString().trim();
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(nameSender, passSender);
            }
        });

        try {
//          Gui email
            Message message = new MimeMessage((session));
            message.setFrom(new InternetAddress(nameSender));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiver));
            message.setSubject("Tìm lại mật khẩu");
            message.setText("Mật khẩu của bạn là: " + password);
            Transport.send(message);
            Toast.makeText(getContext(), "Email send successfully", Toast.LENGTH_LONG).show();
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}