package com.ute.food_express_app.model2;

import java.util.Date;
import java.util.List;

public class Order {

    private int id;
    private Date createdTime;
    private String note;
    private int deliveryCost;
    private Date deliveryTime;
    private Date paymentTime;
    private int customerId;
    private int paymentId;
    private int orderStatus;
    private int discountId;
    private int reasonId;
    private List<OrderDetail> orderDetails;
    private Restaurant restaurant;

    public Order() {
    }

    public Order(int id, Date createdTime, String note, int deliveryCost, Date deliveryTime, Date paymentTime, int customerId, int paymentId, int orderStatus, int discountId, int reasonId) {
        this.id = id;
        this.createdTime = createdTime;
        this.note = note;
        this.deliveryCost = deliveryCost;
        this.deliveryTime = deliveryTime;
        this.paymentTime = paymentTime;
        this.customerId = customerId;
        this.paymentId = paymentId;
        this.orderStatus = orderStatus;
        this.discountId = discountId;
        this.reasonId = reasonId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getDeliveryCost() {
        return deliveryCost;
    }

    public void setDeliveryCost(int deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getDiscountId() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId = discountId;
    }

    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", createdTime=" + createdTime +
                ", note='" + note + '\'' +
                ", deliveryCost=" + deliveryCost +
                ", deliveryTime=" + deliveryTime +
                ", paymentTime=" + paymentTime +
                ", customerId=" + customerId +
                ", paymentId=" + paymentId +
                ", orderStatus=" + orderStatus +
                ", discountId=" + discountId +
                ", reasonId=" + reasonId +
                ", orderDetails=" + orderDetails +
                ", restaurant=" + restaurant +
                '}';
    }
}
