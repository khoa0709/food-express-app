package com.ute.food_express_app.model2;

public class FeedbackImage {
    private int id;
    private String content;
    private int feedbackId;

    public FeedbackImage(int id, String content, int feedbackId) {
        this.id = id;
        this.content = content;
        this.feedbackId = feedbackId;
    }

    public FeedbackImage() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    @Override
    public String toString() {
        return "FeedbackImage{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", feedbackId=" + feedbackId +
                '}';
    }
}
