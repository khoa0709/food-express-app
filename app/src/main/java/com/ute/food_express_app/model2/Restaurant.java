package com.ute.food_express_app.model2;

import java.util.Date;

public class Restaurant {
    private int id;
    private String name;
    private Date openedTime;
    private Date finishedTime;
    private String background;
    private int number;
    private String street;
    private int villageId;

    public Restaurant(int id, String name, Date openedTime, Date finishedTime, String background, int number, String street, int villageId) {
        this.id = id;
        this.name = name;
        this.openedTime = openedTime;
        this.finishedTime = finishedTime;
        this.background = background;
        this.number = number;
        this.street = street;
        this.villageId = villageId;
    }

    public Restaurant() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getOpenedTime() {
        return openedTime;
    }

    public void setOpenedTime(Date openedTime) {
        this.openedTime = openedTime;
    }

    public Date getFinishedTime() {
        return finishedTime;
    }

    public void setFinishedTime(Date finishedTime) {
        this.finishedTime = finishedTime;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getVillageId() {
        return villageId;
    }

    public void setVillageId(int villageId) {
        this.villageId = villageId;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", openedTime=" + openedTime +
                ", finishedTime=" + finishedTime +
                ", background='" + background + '\'' +
                ", number=" + number +
                ", street='" + street + '\'' +
                ", villageId=" + villageId +
                '}';
    }
}
