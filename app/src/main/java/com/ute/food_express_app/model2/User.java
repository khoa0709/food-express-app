package com.ute.food_express_app.model2;

import java.io.Serializable;

public class User implements Serializable {
    private int id;
    private String fullName;
    private String phone;
    private String avatar;
    private int number;
    private String street;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return fullName;
    }

    public void setName(String name) {
        this.fullName = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + fullName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", number=" + number +
                ", street='" + street + '\'' +
                '}';
    }
}
