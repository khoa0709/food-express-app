package com.ute.food_express_app.model2;

import java.util.Date;
import java.util.List;

public class Feedback {
    private int id;
    private int rate;
    private String detail;
    private int restaurantId;
    private Date createTime;
    private User user;
    private List<FeedbackImage> images;

    public Feedback() {
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<FeedbackImage> getImages() {
        return images;
    }

    public void setImages(List<FeedbackImage> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", rate=" + rate +
                ", detail='" + detail + '\'' +
                ", restaurantId='" + restaurantId + '\'' +
                ", createTime=" + createTime +
                ", user=" + user +
                ", images=" + images +
                '}';
    }
}
