package com.ute.food_express_app.model2;

public class Util {
    public static String convertLink(String link) {
        int start = link.indexOf("/d/");
        int finish = link.indexOf("/view");

        String rightLink = "https://drive.google.com/uc?id=";

        rightLink += link.substring(start + 3, finish);

        return rightLink;
    }
}
