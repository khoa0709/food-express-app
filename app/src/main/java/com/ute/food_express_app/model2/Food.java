package com.ute.food_express_app.model2;

public class Food {
    private int id;
    private String name;
    private int price;
    private String image;
    private int restaurantId;

    public Food(int id, String name, int price, String image, int restaurantId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.restaurantId = restaurantId;
    }

    public Food() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", image=" + image +
                ", restaurantId='" + restaurantId + '\'' +
                '}';
    }
}
