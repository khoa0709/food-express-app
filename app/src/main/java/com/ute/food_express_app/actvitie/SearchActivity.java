package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.RestaurantSearchAdapter;
import com.ute.food_express_app.model.Restaurant;
import com.ute.food_express_app.model.RestaurantSearch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

public class SearchActivity extends AppCompatActivity {
    List<Restaurant> listRestaurant;
    RecyclerView rcvRes;
    RestaurantSearchAdapter restaurantSearchAdapter;
    //EditText txtSearch;
    SearchView txtSearch;
    ImageButton btnReturn;
    TextView tbao;
    GifImageView gifImageView;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_search);

        btnReturn = findViewById(R.id.btnReturn);
        txtSearch = findViewById(R.id.search_bar);
        rcvRes = findViewById(R.id.rcv_res);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcvRes.setLayoutManager(linearLayoutManager);
        tbao = findViewById(R.id.tbao);
        gifImageView = findViewById(R.id.giftbao);
        tbao.setVisibility(View.VISIBLE);
        rcvRes.setVisibility(View.GONE);
        gifImageView.setVisibility(View.VISIBLE);

        txtSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                restaurantSearchAdapter = new RestaurantSearchAdapter(getApplicationContext(),getListRestaurantByName(query));
                if(!query.equals("")){
                    tbao.setVisibility(View.GONE);
                    rcvRes.setVisibility(View.VISIBLE);
                    gifImageView.setVisibility(View.GONE);
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rcvRes.setAdapter(restaurantSearchAdapter);
                        }
                    }, 2000);

                }
                else{
                    tbao.setVisibility(View.VISIBLE);
                    rcvRes.setVisibility(View.GONE);
                    gifImageView.setVisibility(View.VISIBLE);

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                restaurantSearchAdapter = new RestaurantSearchAdapter(getApplicationContext(),getListRestaurantByName(newText));
                if(!newText.equals("")){
                    tbao.setVisibility(View.GONE);
                    rcvRes.setVisibility(View.VISIBLE);
                    gifImageView.setVisibility(View.GONE);
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rcvRes.setAdapter(restaurantSearchAdapter);
                        }
                    }, 2000);
                }
                else{
                    tbao.setVisibility(View.VISIBLE);
                    rcvRes.setVisibility(View.GONE);
                    gifImageView.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, MainActivity2.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });
    }

    private List<Restaurant> getListRestaurantByName(String nameRes){
        List<Restaurant> list = new ArrayList<>();
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/restaurant/search/" + nameRes)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++){
                            try {
                                Restaurant restaurant = new Restaurant();
                                JSONObject obj =  response.getJSONObject(i);
                                restaurant.setId(obj.getInt("id"));
                                restaurant.setName(obj.getString("nameRes"));
                                restaurant.setBackgroundUrl(obj.getString("background"));
                                restaurant.setOpenedTime(obj.getString("openedTime"));
                                restaurant.setFinishedTime(obj.getString("finishedTime"));
                                restaurant.setNumber(obj.getString("number"));
                                restaurant.setStreet(obj.getString("street"));
                                restaurant.setVillagedId(obj.getInt("villagedId"));
                                list.add(restaurant);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
        return list;
    }

}