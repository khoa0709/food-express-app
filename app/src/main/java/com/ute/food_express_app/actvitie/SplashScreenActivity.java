package com.ute.food_express_app.actvitie;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ute.food_express_app.R;


public class SplashScreenActivity extends AppCompatActivity {

    private Handler handler;
    ImageView backgroundGradient, backgroundImage, logoImage;
    float v = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        setupAnimation();
    }

    private void setupAnimation() {
        handler = new Handler();

        backgroundGradient = (ImageView) findViewById(R.id.background_gradient);
        backgroundImage = (ImageView) findViewById(R.id.background_image);
        logoImage = (ImageView) findViewById(R.id.img_logo_splash);

        backgroundGradient.setTranslationY(-800);
        backgroundImage.setTranslationY(-800);
        logoImage.setTranslationY(800);

        backgroundGradient.setAlpha(v);
        backgroundImage.setAlpha(v);
        logoImage.setAlpha(v);

        backgroundGradient.animate().translationY(0).alpha(1).setDuration(800).setStartDelay(300).start();
        backgroundImage.animate().translationY(0).alpha(1).setDuration(800).setStartDelay(300).start();
        logoImage.animate().translationY(0).alpha(1).setDuration(800).setStartDelay(500).start();



        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                backgroundGradient.animate().translationY(-1600).alpha(0).setDuration(800).setStartDelay(700).start();
                backgroundImage.animate().translationY(-1600).alpha(0).setDuration(800).setStartDelay(700).start();
                logoImage.animate().translationY(1600).alpha(0).setDuration(800).setStartDelay(900).start();
            }
        }, 1500);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, OnboardingActivity.class));
                finish();
            }
        }, 3000);
    }
}