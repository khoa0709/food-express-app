package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.ute.food_express_app.R;
import com.google.android.material.tabs.TabLayout;

public class AuthenticationActivity extends AppCompatActivity {
    TabLayout tabLayout;
    ViewPager2 viewPager;
    com.ute.food_express_app.adapter.AuthenticationAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_authentication);

        tabLayout = findViewById(R.id.auth_tabs);
        viewPager = findViewById(R.id.auth_views);

        FragmentManager fm = getSupportFragmentManager();
        adapter = new com.ute.food_express_app.adapter.AuthenticationAdapter(fm, getLifecycle());
        viewPager.setAdapter(adapter);

        tabLayout.addTab(tabLayout.newTab().setText("Đăng nhập"));
        tabLayout.addTab(tabLayout.newTab().setText("Đăng ký"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setViewItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });


        SharedPreferences myPref = getApplicationContext().getSharedPreferences("data", MODE_PRIVATE);
        String username = myPref.getString("taikhoan", "");
        String pass = myPref.getString("matkhau", "");
        if(!(username.equals("") && pass.equals(""))) {
            startActivity(new Intent(AuthenticationActivity.this, MainActivity2.class));
            finish();
        }
    }

    public void setViewItem(int item) {
        this.viewPager.setCurrentItem(item);
    }
}