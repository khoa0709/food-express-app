package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ute.food_express_app.R;
import com.google.android.material.tabs.TabLayout;
import com.ute.food_express_app.adapter.SearchListAdapter;

public class ListSearchResActivity extends AppCompatActivity {
    TextView txtSearch;
    ImageView btnBack;
    TabLayout tabLayout;
    ViewPager2 viewPaper;
    SearchListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_list_search_res);

        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        String name = intent.getStringExtra("name");
//        Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();

        txtSearch = (TextView) findViewById(R.id.txt_search);
        btnBack = (ImageView) findViewById(R.id.btn_return);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPaper = (ViewPager2) findViewById(R.id.view_pager);

        txtSearch.setText(name);

        FragmentManager fm = getSupportFragmentManager();
        adapter = new SearchListAdapter(fm, getLifecycle(), id);
        viewPaper.setAdapter(adapter);

        tabLayout.addTab(tabLayout.newTab().setText("Tất cả"));
        tabLayout.addTab(tabLayout.newTab().setText("Đánh giá"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPaper.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPaper.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListSearchResActivity.this, MainActivity2.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                finish();
            }
        });
    }
}