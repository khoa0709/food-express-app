package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.ute.food_express_app.R;
import com.google.android.material.tabs.TabLayout;
import com.ute.food_express_app.adapter.RestaurantVoucherAdapter;
import com.ute.food_express_app.adapter.VoucherDeatilAdapter;
import com.ute.food_express_app.model.RestaurantVoucher;

import java.util.ArrayList;
import java.util.List;

public class VoucherDetailActivity extends AppCompatActivity {
    RecyclerView rcv_voucherDetail;
    RestaurantVoucherAdapter voucherAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_voucher_detail);
        rcv_voucherDetail = findViewById(R.id.rcv_restaurant);
        voucherAdapter = new RestaurantVoucherAdapter(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        rcv_voucherDetail.setLayoutManager(linearLayoutManager);
        voucherAdapter.setData(getListVoucher());
        rcv_voucherDetail.setAdapter(voucherAdapter);
    }
    private List<RestaurantVoucher> getListVoucher(){
        List<RestaurantVoucher> list = new ArrayList<>();
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));
        list.add(new RestaurantVoucher(R.drawable.img_pizza,"Tiệm pizza chú Bảy","02 Thanh Sơn, Thanh Bình", 4.5, "2.3 km"));

        return list;
    }
}