package com.ute.food_express_app.actvitie;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.ute.food_express_app.R;

import java.util.Date;

public class AlarmReceiver extends BroadcastReceiver {
    private static final int NOTIFICATION_ID = 1;
    private static final String CHANNEL_ID = "";
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(context, MainActivity2.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent1,PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.restaurant)
                .setContentTitle("Title")
                .setContentText("Text")
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        notificationManager.notify(0,builder.build());

//        RemoteViews collapsedView = new RemoteViews(getPackageName(),
//                R.layout.notification_layout);
//
//        Intent resultIntent = new Intent(this, MainActivity2.class);
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        stackBuilder.addNextIntentWithParentStack(resultIntent);
//        PendingIntent resultPendingIntent =
//                stackBuilder.getPendingIntent(getNotificationId(), PendingIntent.FLAG_UPDATE_CURRENT);
//
//        android.app.Notification notification = new NotificationCompat.Builder(this, com.ute.food_express_app.actvitie.Notification.CHANNEL_ID)
//                .setSmallIcon(R.drawable.restaurant)
//                .setCustomContentView(collapsedView)
//                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
//                .setContentIntent(resultPendingIntent)
//                .build();
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        if(notificationManager != null){
//            notificationManager.notify(NOTIFICATION_ID, notification);
//        }
//        else{
//            Log.d("hang", "pushNotification: " + notification);
//        }
//    }
//    private int getNotificationId(){
//        return (int) new Date().getTime();
//    }
    }
}
