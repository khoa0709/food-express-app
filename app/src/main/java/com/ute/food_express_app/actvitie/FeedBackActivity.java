package com.ute.food_express_app.actvitie;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;

import com.squareup.picasso.Picasso;
import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.MostOrderRestaurantAdapter;
import com.ute.food_express_app.model.Feedback2;
import com.ute.food_express_app.model.FeedbackImage;
import com.ute.food_express_app.model.Restaurant;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FeedBackActivity extends AppCompatActivity {

    Button add,save;
    LinearLayout layout;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private ImageView imageView,img_res;
    private ArrayList<Bitmap> img_list;
    private ArrayList<String> url_list;
    Bitmap bit;

    //Feedback
    private TextView date;
    private EditText detail;
    private RatingBar rate;
    private Integer num;
    private Button finish;
    //Get ID
    private int count;
    //get date now
    Calendar calendar1 = Calendar.getInstance();
    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
    private int cusId;
    String str = "";
    Integer resId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        add = (Button) findViewById(R.id.btnAdd);
        layout = (LinearLayout) findViewById(R.id.line_layout);
        save = (Button) findViewById(R.id.btnSend);
        finish = (Button) findViewById(R.id.pass);
        //find feedback layout
        date = (TextView) findViewById(R.id.date);
        detail = (EditText) findViewById(R.id.detail);
        rate = (RatingBar) findViewById(R.id.rate);
        num = Math.round(rate.getRating());
        //set date now
        date.setText(simpleDateFormat1.format(calendar1.getTime()));
        img_res = (ImageView) findViewById(R.id.img_res);

        img_list = new ArrayList<>();

        //Get customer Id
        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        cusId = sharedPreferences.getInt("customerId",0);

        Intent intent = getIntent();
        resId = intent.getIntExtra("restaurantId", 0);
        Toast.makeText(this, resId.toString(), Toast.LENGTH_SHORT).show();
//        resId = Integer.parseInt(value1);
        getRestaurantImg();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    imageView = new ImageView(FeedBackActivity.this);
                    imageView.setImageResource(R.drawable.ic_baseline_image_24);
                    selectImage();
                    addImageViews(imageView,300,300);
                }catch (Exception e)
                {
                    Log.e("error", String.valueOf(e));
                }

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(img_list.size()!=0)
                    createFeedback();
                else
                    Toast.makeText(getApplicationContext(),"Vui lòng tải lên ít nhất 1 ảnh",Toast.LENGTH_SHORT).show();
            }
        });
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogShown("Bỏ qua đánh giá","Bạn có chắc muốn đóng, bấm OK để xác nhận",0);
            }
        });
    }

    private void getFeedbackId() {

        String url1 = "http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/feedback";
        AndroidNetworking.get(url1)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int i = response.length()-1;
                        try {
                            JSONObject onj = response.getJSONObject(i);
                            count = onj.getInt("id");
                            uploadData(img_list,count);
                            Log.d("Iddddd", String.valueOf(count));
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.d("loi","loi Id Customer");
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny

                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(FeedBackActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(FeedBackActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    layout.removeView(imageView);
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }

        } else {
            layout.removeView(imageView);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageView.setImageBitmap(thumbnail);
        bit = thumbnail;
        img_list.add(bit);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageView.setImageBitmap(bm);
        bit = bm;
        img_list.add(bit);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public void addImageViews(ImageView imgV, int with, int height) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(with, height);
        layoutParams.setMargins(0, 10, 10, 0);
        imgV.setLayoutParams(layoutParams);
        layout.addView(imgV);
    }

    void createFeedback() {
        Feedback2 fe = new Feedback2();
        Date date1 = null;
        fe.setDetail(detail.getText().toString());
        fe.setRate(Math.round(rate.getRating()));
        //Truyen zo
        fe.setRestaurantId(resId);
        fe.setCustomerId(cusId);

        //format date
        String datestr = simpleDateFormat2.format(calendar1.getTime());

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customerId", fe.getCustomerId());
            jsonObject.put("rate", fe.getRate());
            jsonObject.put("detail", fe.getDetail());
            jsonObject.put("createdTime", datestr);
            jsonObject.put("restaurantId", fe.getRestaurantId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Feedback",jsonObject.toString());

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/feedback/createFeedback")
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
                        getFeedbackId();
                        dialogShown("Đánh giá đã được gửi thành công", "Cảm ơn phản hồi của bạn, chúc bạn ngon miệng!!", 1);

                    }
                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), anError.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    void createFeedbackImage(String url, int feedId) {
        FeedbackImage feImg = new FeedbackImage();
        feImg.setContent(url);
        feImg.setFeedbackId(feedId);
        Log.d("String URL", feImg.getContent());
        Log.d("String ID", String.valueOf(feImg.getFeedbackId()));

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("content", feImg.getContent());
            jsonObject.put("feedbackId", feImg.getFeedbackId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Image Feedback",jsonObject.toString());

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/feedbackImage/createFeedbackImage")
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), response.toString() + "Image Feedback", Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), anError.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    void uploandImg(Bitmap bm, int cusid) {
        String key = "61ee5e9486c821378fdbb1a5a312772c";
        String url = "https://api.imgbb.com/1/upload?key="+key;
        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        Uri tempUri = getImageUri(getApplicationContext(), bm);

        // CALL THIS METHOD TO GET THE ACTUAL PATH
        File finalFile = new File(getRealPathFromURI(tempUri));
        AndroidNetworking.upload(url)
                .addMultipartFile("image", finalFile)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    String imageUrl;

                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            JSONObject data = response.getJSONObject("data");
                            imageUrl = data.getString("url");
                            //upload Image susscess create FeedbackImage
                            createFeedbackImage(imageUrl, cusid);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.e("Error Upload Image", String.valueOf(error));
                    }
                });
    }

    void uploadData(ArrayList<Bitmap> img, int id) {
        for (Bitmap value : img) {
            uploandImg(value, id);
        }
    }

    void dialogShown(String title, String mess, int type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(FeedBackActivity.this);

        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(mess);

        if (type == 0) {
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
        }

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(FeedBackActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });
        builder.show();
    }
    private void getRestaurantImg() {

        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/restaurant/id/"+resId)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i< response.length();i++)
                        {

                            try {
                                JSONObject onj = response.getJSONObject(i);
                                str = onj.getString("background");
                            }catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }

                        Picasso.get().load(str).into(img_res);

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}