package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ute.food_express_app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class NotificationActivity extends AppCompatActivity {
    private static final int NOTIFICATION_ID = 1;
    private static final String CHANNEL_ID = "";
    Button btnPush;
    TextView txtTimer, txtContent;
    String title, content, imageUrl, createdTime;
    ArrayList<String> arrayTitle, arrayContent, arrayImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        btnPush = findViewById(R.id.btnPush);
        txtTimer = findViewById(R.id.timer);
        txtContent = findViewById(R.id.content);

        btnPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer();
            }
        });
    }

    public void timer(){
        //long duration = TimeUnit.MINUTES.toMillis(1);
        long duration = TimeUnit.SECONDS.toMillis(10);
        new CountDownTimer(duration,1){
            @Override
            public void onTick(long l) {
                String sDuration = String.format(Locale.ENGLISH, "%02d : %02d"
                ,TimeUnit.MILLISECONDS.toMinutes(l)
                ,TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l)));

                txtTimer.setText(sDuration);
            }

            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                randomData();
                pushNotification();
                addData();
            }
        }.start();
    }

    public void pushNotification(){
        RemoteViews collapsedView = new RemoteViews(getPackageName(),
                R.layout.notification_layout);

        Intent resultIntent = new Intent(this, MainActivity2.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(getNotificationId(), PendingIntent.FLAG_UPDATE_CURRENT);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, title);
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, content);

        Notification notification = new NotificationCompat.Builder(this, com.ute.food_express_app.actvitie.App.CHANNEL_ID)
                .setSmallIcon(R.drawable.restaurant)
                .setCustomContentView(collapsedView)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setContentIntent(resultPendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager != null){
            notificationManager.notify(NOTIFICATION_ID, notification);
        }
        else{
            Log.d("hang", "pushNotification: " + notification);
        }
    }

    private int getNotificationId(){
        return (int) new Date().getTime();
    }

    public void addData(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", title);
            jsonObject.put("content", content);
            jsonObject.put("imageUrl", imageUrl);
            jsonObject.put("createdTime", createdTime);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/announcement/createAnnouncement")
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(getApplicationContext(), response.getString("status"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void randomData(){
        //Get random list title, list content, git imageURL
        String[] listTitle = getResources().getStringArray(R.array.listTitle);
        String[] listContent = getResources().getStringArray(R.array.listContent);
        String[] listImage = getResources().getStringArray(R.array.listImage);
        arrayTitle = new ArrayList<>(Arrays.asList(listTitle));
        arrayContent = new ArrayList<>(Arrays.asList(listContent));
        arrayImage = new ArrayList<>(Arrays.asList(listImage));

        Collections.shuffle(arrayTitle);
        Collections.shuffle(arrayContent);
        Collections.shuffle(arrayImage);

        title = arrayTitle.get(7);
        content = arrayContent.get(7);
        imageUrl = arrayImage.get(7);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());
        createdTime = df.format(Calendar.getInstance().getTime());
        txtContent.setText(createdTime);
    }
}