package com.ute.food_express_app.actvitie;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.ute.food_express_app.R;
import com.ute.food_express_app.fragment.BasketFragment;
import com.ute.food_express_app.fragment.FavoriteFragment;
import com.ute.food_express_app.fragment.HomeFragment;
import com.ute.food_express_app.fragment.NotificationFragment;
import com.ute.food_express_app.fragment.UserFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MainActivity2 extends AppCompatActivity {
    TextView txtAllCollections;
    MeowBottomNavigation bottomNavigation;
    int cusId;
    String data;
    boolean favourite;

    private static final int NOTIFICATION_ID = 1;
    private static final String CHANNEL_ID = "";
    //Button btnPush;
    //TextView txtTimer;
    String title, content, imageUrl, createdTime;
    ArrayList<String> arrayTitle, arrayContent, arrayImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        data = intent.getStringExtra("data");
        favourite = intent.getBooleanExtra("favourite", false);

        //Collections
        txtAllCollections = findViewById(R.id.all_collection);

        //Footer
        bottomNavigation = findViewById(R.id.bottom_natigation);
        //Add footer item
        bottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.ic_basket1));
        bottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.ic_favorite));
        bottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.ic_home1));
        bottomNavigation.add(new MeowBottomNavigation.Model(4, R.drawable.ic_notification1));
        bottomNavigation.add(new MeowBottomNavigation.Model(5, R.drawable.ic_person1));

        bottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {
                Fragment fragment = null;
                switch (item.getId()){
                    case 1:
                        fragment = new BasketFragment();
                        break;
                    case 2:
                        fragment = new FavoriteFragment();
                        break;
                    case 3:
                        fragment = new HomeFragment(data);
                        break;
                    case 4:
                        fragment = new NotificationFragment();
                        break;
                    case 5:
                        fragment = new UserFragment();
                        break;
                }

                loadFragment(fragment);
            }
        });
        if(favourite) {
            loadFragment(new FavoriteFragment());
            bottomNavigation.setCount(4, "10");
            bottomNavigation.show(2,true);
            finish();
        }


        //Get User Fragment
        Intent intent2 = getIntent();
        int value2 = intent2.getIntExtra("Key_UserInforFragment", 0);

        bottomNavigation.setCount(4, "10");
        if(value2 != 0)
            bottomNavigation.show(value2,true);
        else
            bottomNavigation.show(3,true);


        bottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {

            }
        });

        bottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {

            }
        });

        //push notification
        timer();

    }
    private void loadFragment(Fragment fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.Header,fragment)
                .commit();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    public void timer(){
        //long duration = TimeUnit.MINUTES.toMillis(5);
        long duration = TimeUnit.SECONDS.toMillis(360);
        new CountDownTimer(duration,1){
            @Override
            public void onTick(long l) {
                String sDuration = String.format(Locale.ENGLISH, "%02d : %02d"
                        ,TimeUnit.MILLISECONDS.toMinutes(l)
                        ,TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l)));
            }

            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                randomData();
                pushNotification();
                addData();
            }
        }.start();
    }

    public void pushNotification(){
        RemoteViews collapsedView = new RemoteViews(getPackageName(),
                R.layout.notification_layout);

        Intent resultIntent = new Intent(this, MainActivity2.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(getNotificationId(), PendingIntent.FLAG_UPDATE_CURRENT);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, title);
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, content);

        android.app.Notification notification = new NotificationCompat.Builder(this, com.ute.food_express_app.actvitie.App.CHANNEL_ID)
                .setSmallIcon(R.drawable.restaurant)
                .setCustomContentView(collapsedView)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setContentIntent(resultPendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager != null){
            notificationManager.notify(NOTIFICATION_ID, notification);
        }
        else{
            Log.d("hang", "pushNotification: " + notification);
        }
    }

    private int getNotificationId(){
        return (int) new Date().getTime();
    }

    public void addData(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", title);
            jsonObject.put("content", content);
            jsonObject.put("imageUrl", imageUrl);
            jsonObject.put("createdTime", createdTime);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/announcement/createAnnouncement")
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(getApplicationContext(), response.getString("status"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void randomData(){
        //Get random list title, list content, git imageURL
        String[] listTitle = getResources().getStringArray(R.array.listTitle);
        String[] listContent = getResources().getStringArray(R.array.listContent);
        String[] listImage = getResources().getStringArray(R.array.listImage);
        arrayTitle = new ArrayList<>(Arrays.asList(listTitle));
        arrayContent = new ArrayList<>(Arrays.asList(listContent));
        arrayImage = new ArrayList<>(Arrays.asList(listImage));

        Collections.shuffle(arrayTitle);
        Collections.shuffle(arrayContent);
        Collections.shuffle(arrayImage);

        title = arrayTitle.get(7);
        content = arrayContent.get(7);
        imageUrl = arrayImage.get(7);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        createdTime = df.format(Calendar.getInstance().getTime());
    }
}