package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.OnboardingAdapter;
import com.ute.food_express_app.model.OnboardingItem;

import java.util.ArrayList;
import java.util.List;

public class OnboardingActivity extends AppCompatActivity {

    private OnboardingAdapter onboardingAdapter;
    private LinearLayout layoutOnboardingIndicators;
    private Button buttonOnboardingSkip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        if(restorePrefData()) {
            Intent homeActivity = new Intent(getApplicationContext(), AuthenticationActivity.class);
            startActivity(homeActivity);
            finish();
        }

        setContentView(R.layout.activity_onboarding);
        layoutOnboardingIndicators = findViewById(R.id.layout_onboarding_indicators);
        buttonOnboardingSkip = findViewById(R.id.button_onboarding_skip);
        setupOnboardingItems();

        ViewPager2 onboardingViewPager = findViewById(R.id.onboarding_viewpager);
        onboardingViewPager.setAdapter(onboardingAdapter);

        setupOnboardingIndicators();
        setCurrentOnboardingIndicator(0);

        onboardingViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setCurrentOnboardingIndicator(position);
            }
        });

        buttonOnboardingSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OnboardingActivity.this, AuthenticationActivity.class));
                savePrefsData();
                finish();
            }
        });
    }

    private boolean restorePrefData() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        Boolean isOnboardingOpenBefore = pref.getBoolean("isOnboardingOpened", false);
        return isOnboardingOpenBefore;
    }

    private void setupOnboardingItems() {
        List<OnboardingItem> onboardingItemList = new ArrayList<>();

        onboardingItemList.add(new OnboardingItem(R.drawable.img_logo, R.drawable.img_onboarding_vector_1, "Khám phá", "những địa điểm, quán ăn mới ngay gần bạn"));
        onboardingItemList.add(new OnboardingItem(R.drawable.img_logo, R.drawable.img_onboarding_vector_2, "Vô vàn", "những món ăn hấp dẫn sẽ đánh thức vị giác của bạn"));
        onboardingItemList.add(new OnboardingItem(R.drawable.img_logo, R.drawable.img_onboarding_vector_3, "Giao hàng", "tận nơi và nhanh chóng với đội ngũ nhân viên chuyên nghiệp"));

        onboardingAdapter = new OnboardingAdapter(onboardingItemList);

    }

    private void setupOnboardingIndicators() {
        ImageView[] indicators = new ImageView[onboardingAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(0, 0, 15, 0);
        for(int i = 0; i < indicators.length; i++) {
            indicators[i] = new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(
                    getApplicationContext(),
                    R.drawable.onboarding_indicator_inactive
            ));
            indicators[i].setLayoutParams(layoutParams);
            layoutOnboardingIndicators.addView(indicators[i]);
        }

    }

    private void setCurrentOnboardingIndicator(int index) {
        int childCount = layoutOnboardingIndicators.getChildCount();
        for(int i = 0; i< childCount; i++) {
            ImageView imageView = (ImageView) layoutOnboardingIndicators.getChildAt(i);
            if(i == index) {
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(), R.drawable.onboarding_indicator_active)
                );
            } else {
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(), R.drawable.onboarding_indicator_inactive)
                );
            }
        }
        if(index == onboardingAdapter.getItemCount() - 1) {
            buttonOnboardingSkip.setText("Bắt đầu");
        } else {
            buttonOnboardingSkip.setText("Bỏ qua");
        }
    }

    private void savePrefsData() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isOnboardingOpened", true);
        editor.commit();

    }
}