package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.ute.food_express_app.R;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.model.Customer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserInforActivity extends AppCompatActivity {
    ImageButton btn_return,btn_change;
    Button btn_save;
    CheckBox ck_male, ck_female;
    ConstraintLayout layout;
    ImageView img;
    float v = 0;
    Customer cusLogin,cusUpdate;
    Integer checkChangeImg=0;
    String str = "01/01/2001";


    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private Bitmap bit=null;

    //elements Customer
    private ImageView imgLogo;
    private EditText edCustomerName;
    private EditText edPass;
    private EditText edDob;
    private EditText edEmail;
    private EditText edPhone;
    private String imageUrl="";

    private static Pattern pattern;
    private Matcher matcher;
    private static final String EMAIL_REGEX = "^[A-Za-z0-9]+[A-Za-z0-9]*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)$";
    private static final String PHONE_REGEX = "^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_infor);
        Intent intent = getIntent();
        cusLogin = (Customer) intent.getSerializableExtra("user");

        //find edits of Customer
        imgLogo = findViewById(R.id.imgLogo);
        edCustomerName = findViewById(R.id.edit_name);
        edDob = findViewById(R.id.edit_date);
        edEmail = findViewById(R.id.edit_email);
        edPhone = findViewById((R.id.edit_phone));
        edPass = findViewById((R.id.edit_pass));
        ck_male = (CheckBox) findViewById(R.id.selectNam);
        ck_female = (CheckBox) findViewById(R.id.selectNu);

        //set data
        String urlImg = cusLogin.getAvatar();
        Picasso.get().load(urlImg).into(imgLogo);
        edCustomerName.setText(cusLogin.getFullName());

        edPhone.setText(String.valueOf(cusLogin.getPhone()));
        edPass.setText(String.valueOf(cusLogin.getPass()));

        if(cusLogin.getDob().equals("Chưa có"))
        {

        }
        else
            edEmail.setText(String.valueOf(cusLogin.getEmail()));

        if(cusLogin.getDob().equals("Chưa có"))
        {

        }
        else
            edDob.setText(String.valueOf(cusLogin.getDob()));


        if(cusLogin.getGender().equals("M"))
        {
            ck_male.setChecked(true);
            ck_female.setChecked(false);
        }
        else
        {
            ck_female.setChecked(true);
            ck_male.setChecked(false);
        }

        btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_save = (Button) findViewById(R.id.save);
        btn_change = (ImageButton) findViewById(R.id.btn_change);

        ck_male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    ck_female.setChecked(false);
                    ck_female.setTextColor(getResources().getColor(R.color.black));
                    ck_male.setTextColor(getResources().getColor(R.color.primary_background));
                }
                else
                {
                    ck_male.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        ck_female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    ck_male.setChecked(false);
                    ck_male.setTextColor(getResources().getColor(R.color.black));
                    ck_female.setTextColor(getResources().getColor(R.color.primary_background));
                }
                else
                {
                    ck_female.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });
        edPass.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent event) {
                if(i == KeyEvent.KEYCODE_ENTER)
                {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        edPass.clearFocus();
                        edPhone.requestFocus();
                        edPhone.setCursorVisible(true);
                    }
                    return true;
                }
                return false;
            }
        });
        edPhone.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent event) {
                if(i == KeyEvent.KEYCODE_ENTER)
                {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        edPhone.clearFocus();
                        edEmail.requestFocus();
                        edEmail.setCursorVisible(true);
                    }
                    return true;
                }
                return false;
            }
        });
        edDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                {
                    ChonNgay();
                }
            }
        });
        edDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChonNgay();
            }
        });

        //set hieu ung
        layout = (ConstraintLayout) findViewById(R.id.layout);
        img = (ImageView) findViewById(R.id.img);

        layout.setTranslationY(300);
        layout.setAlpha(v);
        layout.animate().translationY(0).alpha(1).setDuration(1400).start();

        img.setTranslationY(100);
        img.setAlpha(v);
        img.animate().translationY(0).alpha(1).setDuration(1600).start();

        //doi anh
        btn_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(edCustomerName.getText()))
                {
                    Toast.makeText(getApplicationContext(),"Không được để trống Tên",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edEmail.getText()))
                {
                    Toast.makeText(getApplicationContext(),"Không được để trống Email",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edDob.getText()))
                {
                    Toast.makeText(getApplicationContext(),"Không được để trống Ngày sinh",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edPass.getText()))
                {
                    Toast.makeText(getApplicationContext(),"Không được để trống Mật khẩu",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edPhone.getText()))
                {
                    Toast.makeText(getApplicationContext(),"Không được để trống Số điện thoại",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (validate((edPhone.getText().toString()),PHONE_REGEX)==false)
                {
                    Toast.makeText(getApplicationContext(),"Số điện thoại ko hợp lệ",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (validate((edEmail.getText().toString()),EMAIL_REGEX)==false)
                {
                    Toast.makeText(getApplicationContext(),"Email ko hợp lệ",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(bit==null)
                {
                    update(cusLogin.getAvatar());
                }
                else
                {
                    uploandImg(bit);
                }
                //go back
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent1 = new Intent(UserInforActivity.this,MainActivity2.class);
                        intent1.putExtra("Key_UserInforFragment", 5);
                        startActivity(intent1);
                        finish();
                    }
                }, 5000);
            }
        });
    }
    public boolean validate(String regex,String pa) {
        pattern = Pattern.compile(pa);
        matcher = pattern.matcher(regex);
        return matcher.matches();
    }


    private void ChonNgay(){
        Date date = null;

        if(TextUtils.isEmpty(edDob.getText().toString()))
        {
//            str = "01/01/2000";
        }
        else
        {
            str = edDob.getText().toString();
        }

        try {
            date =  new SimpleDateFormat("dd/MM/yyyy").parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int ngay = calendar.get(Calendar.DATE);
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                calendar.set(i,i1,i2);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                edDob.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },nam,thang,ngay);
        datePickerDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(UserInforActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(UserInforActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_FILE)
            {
                onSelectFromGalleryResult(data);
            }

            else if (requestCode == REQUEST_CAMERA)
            {
                onCaptureImageResult(data);
            }

        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        imgLogo.setImageBitmap(thumbnail);
        bit = thumbnail;
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imgLogo.setImageBitmap(bm);
        bit = bm;
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }
    void uploandImg(Bitmap bm){
        String keyHost = "61ee5e9486c821378fdbb1a5a312772c";
        String url = "https://api.imgbb.com/1/upload?key="+keyHost;
        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        Uri tempUri = getImageUri(getApplicationContext(), bm);

        // CALL THIS METHOD TO GET THE ACTUAL PATH
        File finalFile = new File(getRealPathFromURI(tempUri));
        AndroidNetworking.upload(url)
                .addMultipartFile("image",finalFile)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            JSONObject data = response.getJSONObject("data");
                            imageUrl = data.getString("url");
                            update(imageUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.d("TAG_LOG", String.valueOf(error));
                    }
                });
    }


    void update(String url){
        String gen;
        if(ck_male.isChecked())
            gen = "M";
        else
            gen = "F";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fullName", edCustomerName.getText().toString());
            jsonObject.put("phone", edPhone.getText().toString());
            jsonObject.put("gender", gen);
            jsonObject.put("dob",edDob.getText().toString());
            jsonObject.put("email", edEmail.getText().toString());
            jsonObject.put("pass", edPass.getText().toString());
            jsonObject.put("avatar", url);
            jsonObject.put("street", "");
            jsonObject.put("number", "");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/customer/updateCustomer/"+cusLogin.getId())
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // do anything with response
                        try {
                            JSONObject obj = response.getJSONObject(0);
                            Toast
                                    .makeText(UserInforActivity.this,
                                            "Cập nhật thành công!!",
                                            Toast.LENGTH_SHORT)
                                    .show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

}