package com.ute.food_express_app.actvitie;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ute.food_express_app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class CancellationActivity extends AppCompatActivity {

    private ImageButton btnReturn;
    private Button btnSendReason;
    int orderId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cancellation);

        Intent intent = getIntent();
        orderId = intent.getIntExtra("orderId", 0);

        mapping();

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CancellationActivity.this, OrderDetailActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        btnSendReason = findViewById(R.id.btnSendReason);
        btnSendReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateOrderStatus();

            }
        });
    }

    public void mapping(){
        btnReturn = (ImageButton) findViewById(R.id.btnReturn);
    }

    public void updateOrderStatus(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", String.valueOf(orderId));
            jsonObject.put("orderStatus", String.valueOf(6));

            Log.v("Duong 111111111111111: ", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/order/updateOrder")
                .addJSONObjectBody(jsonObject)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Toast.makeText(CancellationActivity.this, "Cancel successfully", Toast.LENGTH_SHORT).show();
//                        Log.v("Duong success: ", response.toString());
                        pushNotification();
                        Intent intent = new Intent(CancellationActivity.this, MainActivity2.class);
//                        intent.putExtra("viewpage_position", "1");
                        CancellationActivity.this.finish();
                        startActivity(intent);
                    }
                    @Override
                    public void onError(ANError error) {
                        Log.v("Duong fail: ", error.toString());
                    }
        });

    }

    public void pushNotification(){
        RemoteViews collapsedView = new RemoteViews(getPackageName(),
                R.layout.order_notification_layout);

        Intent resultIntent = new Intent(this, MainActivity2.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(getNotificationId(), PendingIntent.FLAG_UPDATE_CURRENT);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, "Hủy đơn hàng thành công");
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, "Đơn hàng của bạn đã được hủy.Bạn có thể đặt lại đơn hàng tại đây.");

        Notification notification = new NotificationCompat.Builder(this, com.ute.food_express_app.actvitie.App.CHANNEL_ID)
                .setSmallIcon(R.drawable.restaurant)
                .setCustomContentView(collapsedView)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setContentIntent(resultPendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager != null){
            notificationManager.notify(1, notification);
        }
        else{
            Log.d("hang", "pushNotification: " + notification);
        }
    }

    private int getNotificationId(){
        return (int) new Date().getTime();
    }

}
