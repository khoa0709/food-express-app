package com.ute.food_express_app.actvitie;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ute.food_express_app.R;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.ute.food_express_app.adapter.CartFoodAdapter;
import com.ute.food_express_app.adapter.RestaurantDetailTabAdapter;
import com.ute.food_express_app.fragment.HomeFragment;
import com.ute.food_express_app.fragment.Set_Menu_Fragment;
import com.ute.food_express_app.model.Food;
import com.ute.food_express_app.model.Restaurant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RestaurantDetailActivity extends AppCompatActivity {

    TabLayout res_detail_tabLayout;
    ViewPager2 res_detail_viewPager;
    RestaurantDetailTabAdapter adapter;
    ImageView imgResBackground, iconHeart;
    TextView txtRestaurantName;
    ImageButton btn_back;
//    FrameLayout layoutCart;

    ConstraintLayout layoutCart;
    ConstraintLayout layoutCartTemporary;

    RecyclerView rcv_listFoodCartTemp;
    ArrayList<Food> listFoodTemp = new ArrayList<>();
    CartFoodAdapter cartFoodAdapter;
    SharedPreferences sharedPreferences;
    int restaurantId;
    int customerId;
    int isFromFvt;
    boolean isFavourite;
    List<Restaurant> listFavourite;
    Restaurant restaurant;

    String resaurantName;

    float v = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_restaurant_detail);

        Intent intent = this.getIntent();
        isFromFvt = intent.getIntExtra("isFromFvt", 0);
        restaurantId = intent.getIntExtra("restaurantId", 2);
        resaurantName = intent.getStringExtra("restaurantName");

        restaurant = new Restaurant();
        listFavourite = new ArrayList<>();
        sharedPreferences = this.getSharedPreferences("data", Context.MODE_PRIVATE);
        customerId = sharedPreferences.getInt("customerId", 0);


        res_detail_tabLayout = findViewById(R.id.res_detail_tabLayout);
        res_detail_viewPager = findViewById(R.id.res_detail_viewpager);
        imgResBackground = findViewById(R.id.img_background_restaurant);
        txtRestaurantName = findViewById(R.id.txtRestaurantName);

        layoutCart = (ConstraintLayout) findViewById(R.id.cart);
        layoutCart.setVisibility(View.INVISIBLE);

        layoutCartTemporary = (ConstraintLayout) findViewById(R.id.cart_temporary);
        layoutCartTemporary.setVisibility(View.INVISIBLE);

//        rcv_listFoodCartTemp = findViewById(R.id.rcv_listFoodCartTemp);
//        rcv_listFoodCartTemp.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//        cartFoodAdapter = new CartFoodAdapter(getApplicationContext(), listFoodTemp);
//        rcv_listFoodCartTemp.setAdapter(adapter);
        iconHeart = findViewById(R.id.icon_heart);
        isFavourite = false;



        loadData();

        FragmentManager fm = getSupportFragmentManager();
        adapter = new RestaurantDetailTabAdapter(fm, getLifecycle(), getApplicationContext(), layoutCart, layoutCartTemporary);
        res_detail_viewPager.setAdapter(adapter);

        res_detail_tabLayout.addTab(res_detail_tabLayout.newTab().setText("Đặt đơn"));
        res_detail_tabLayout.addTab(res_detail_tabLayout.newTab().setText("Bình luận"));
        res_detail_tabLayout.addTab(res_detail_tabLayout.newTab().setText("Thông tin"));
        res_detail_tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        res_detail_tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setViewItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        res_detail_viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                res_detail_tabLayout.selectTab(res_detail_tabLayout.getTabAt(position));
//                super.onPageSelected(position);
            }
        });

        iconHeart.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                iconHeart.setVisibility(View.INVISIBLE);
                if(isFavourite) {
                    removeFromFavourite();
                    changeHeartIcon();
                } else {
                    addToFavourite();
                    changeHeartIcon();
                }
            }
        });


        btn_back = findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFromFvt == 1) {
                    Intent intent = new Intent(RestaurantDetailActivity.this, MainActivity2.class);
                    intent.putExtra("favourite", true);
                    isFromFvt = 0;
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(RestaurantDetailActivity.this, MainActivity2.class);
                    startActivity(intent);
                    finish();
                }

//                Intent intent = new Intent(RestaurantDetailActivity.this, MainActivity2.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
//                finish();

            }
        });

    }

    public void setViewItem(int item) {
        this.res_detail_viewPager.setCurrentItem(item);
    }


    //Duong: load data to restaurant
    public void loadData(){
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/favourite/{id}")
                .addPathParameter("id", String.valueOf(customerId))
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i=0;i< response.length();i++)
                        {
                            try {
                                JSONObject onj = response.getJSONObject(i);
                                if(restaurantId == onj.getInt("restaurantId")) {
                                    isFavourite = true;
                                    break;
                                }
                            }catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });

        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/restaurant/id/{id}")
                .addPathParameter("id", String.valueOf(restaurantId))
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Restaurant restaurant = new Restaurant();
                            JSONObject obj = response.getJSONObject(0);
                            restaurant.setId(obj.getInt("id"));
                            restaurant.setName(obj.getString("nameRes"));
                            restaurant.setOpenedTime(obj.getString("openedTime"));
                            restaurant.setFinishedTime(obj.getString("finishedTime"));
                            restaurant.setBackgroundUrl(obj.getString("background"));
                            restaurant.setNumber(obj.getString("number"));
                            restaurant.setStreet(obj.getString("street"));
                            restaurant.setVillagedId(obj.getInt("villagedId"));
                            restaurant.setMenuCategoryId(obj.getInt("menuCategoryId"));

                            txtRestaurantName.setText(restaurant.getName());
                            Picasso.get().load(restaurant.getBackgroundUrl()).into(imgResBackground);
                            changeHeartIcon();

                            SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("restaurantName", restaurant.getName());
                            editor.apply();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.v("Error Duongggggggggg", anError.toString());
                    }
                });
    }

    public void addToFavourite() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("restaurantId", restaurantId);
            jsonObject.put("customerId", customerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/favourite/createFavourite")
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            isFavourite = true;
                            Toast.makeText(RestaurantDetailActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

        public int getRestaurantId(){
        return this.restaurantId;
    }

    public String getResaurantName() {
        return this.resaurantName;
    }

    public void removeFromFavourite() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("restaurantId", restaurantId);
            jsonObject.put("customerId", customerId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/favourite/deleteFavourite")
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            isFavourite = false;
                            Toast.makeText(RestaurantDetailActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void changeHeartIcon() {
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isFavourite) {
                    iconHeart.setImageDrawable(getDrawable(R.drawable.icon_add_favourite_filled));
                    iconHeart.setVisibility(View.VISIBLE);
                } else {
                    iconHeart.setImageDrawable(getDrawable(R.drawable.icon_add_favourite_empty));
                    iconHeart.setVisibility(View.VISIBLE);
                }
            }
        }, 1500);
    }

}