package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;

import com.ute.food_express_app.R;

public class YourLocation2Activity extends AppCompatActivity {
    ImageButton btn_return;
    EditText edit_seach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_your_location2);


        btn_return = (ImageButton) findViewById(R.id.btn_return);
        edit_seach = (EditText) findViewById(R.id.edit_seach);

        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(YourLocation2Activity.this, YourLocation1Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        edit_seach.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_ENTER ) {
                    if (event.getAction() == KeyEvent.ACTION_UP) {
                        Intent intent = new Intent(YourLocation2Activity.this, YourLocation3Activity.class);
                        intent.putExtra("Key_1", edit_seach.getText().toString());
                        startActivity(intent);
                    }
                    return true;
                }
                return false;
            }
        });


    }
}