package com.ute.food_express_app.actvitie;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.OrderDetailAdapter;
import com.ute.food_express_app.fragment.BasketFragment;
import com.ute.food_express_app.model.Food;
import com.ute.food_express_app.model.MyFood;
import com.ute.food_express_app.model2.User;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderDetailActivity extends AppCompatActivity {

    private Button btnCancelOrder, btnToCart;
    private ImageButton btnReturn;
    private TextView txtName,txtPhone, txtOrderAddress, txtSumOfPrice,
            txtDeliveryCost, txtTotalCost, txtDiscount, txtSale, txtTime, txtDate;

    private List<Food> foodList;
    private User user;

    static final int DELIVERY_COST = 20000;
    int orderId;

    int discount = 0;
    String code = "";

    DecimalFormat formatter = new DecimalFormat("###,###,###");

    int minute = 5;
    int restaurantId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.actitvity_order_detail);

        mapping();

        Bundle bundle = getIntent().getBundleExtra("order");
        foodList = (List<Food>) bundle.getSerializable("foodList");
        restaurantId = foodList.get(0).getRestaurantId();
        user = (User) bundle.getSerializable("user");

        if(bundle.containsKey("discount")){
            discount = Integer.parseInt(bundle.get("discount").toString());
        }
        if(bundle.containsKey("code")){
            code = bundle.getString("code");
        }

        txtName.setText(user.getName());
        txtPhone.setText("0" + user.getPhone());
        txtOrderAddress.setText(user.getStreet());

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dateStr = format.format(date);

        date.setTime(date.getTime() + 15*60*1000);

        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm");


        txtDate.setText(dateStr);
        txtTime.setText(timeFormat.format(date));

        setValue();

        RecyclerView rcvOrderDetailFood = findViewById(R.id.rcvOrderDetailFood);
        OrderDetailAdapter orderFoodAdapter = new OrderDetailAdapter(this, foodList);
        rcvOrderDetailFood.setAdapter(orderFoodAdapter);
        LinearLayoutManager vertical = new LinearLayoutManager(OrderDetailActivity.this, LinearLayoutManager.VERTICAL, false);
        rcvOrderDetailFood.setLayoutManager(vertical);

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pushNotification();
                btnCancelOrder.setVisibility(View.INVISIBLE);
                btnToCart.setVisibility(View.VISIBLE);
            }
        }, 10000);

        //btnCancelOrder click
        btnCancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacksAndMessages(null);
                Bundle bundle = getIntent().getBundleExtra("order");
                orderId = bundle.getInt("orderId", 0);
                Intent intent = new Intent(OrderDetailActivity.this, CancellationActivity.class);
                intent.putExtra("orderId", orderId);
                OrderDetailActivity.this.finish();
                startActivity(intent);
            }
        });

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderDetailActivity.this, MapActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        btnToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderDetailActivity.this, MainActivity2.class);
                startActivity(intent);
            }
        });
    }

    public void mapping(){
        btnCancelOrder = (Button) findViewById(R.id.btnCancelOrder);
        btnReturn = (ImageButton) findViewById(R.id.btnReturn);
        btnToCart = findViewById(R.id.btnToCart);

        txtName = findViewById(R.id.txtName);
        txtPhone = findViewById(R.id.txtPhone);
        txtOrderAddress = findViewById(R.id.txtOrderAddress);
        txtSumOfPrice = findViewById(R.id.txtSumOfPrice);
        txtTotalCost = findViewById(R.id.txtTotalCost);
        txtDeliveryCost = findViewById(R.id.txtShippingCost);

        txtDiscount = findViewById(R.id.txtDiscount);
        txtSale = findViewById(R.id.txtSale);
        txtTime = findViewById(R.id.txtTime);
        txtDate = findViewById(R.id.txtDate);
    }

    public void setValue(){
        txtSumOfPrice = findViewById(R.id.txtSumOfPrice);
        txtDeliveryCost = findViewById(R.id.txtShippingCost);
        txtTotalCost = findViewById(R.id.txtTotalCost);

        if(discount != 0){
            txtDiscount.setText(formatter.format(discount));
            txtSale.setText("Giảm giá (" + code + ")");
        }

        txtSumOfPrice.setText(formatter.format(getSumOfPrice(foodList)));
        txtDeliveryCost.setText(formatter.format(DELIVERY_COST));
        txtTotalCost.setText(formatter.format(getSumOfPrice(foodList) + DELIVERY_COST - discount));


    }

    public int getSumOfPrice(List<Food> foodList){
        int priceTotal= 0;
        for(Food food : foodList){
            priceTotal += food.getQuantityOrder() * food.getPrice();
        }
        return priceTotal;
    }

    public int getDiscountPrice() {
        return 0;
    }

    public void pushNotification(){
        RemoteViews collapsedView = new RemoteViews(getPackageName(),
                R.layout.order_notification_layout);

//        Intent resultIntent = new Intent(this, MainActivity2.class);
        Intent resultIntent = new Intent(this, FeedBackActivity.class);
        resultIntent.putExtra("restaurantId", restaurantId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(getNotificationId(), PendingIntent.FLAG_UPDATE_CURRENT);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, "Giao hàng");
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, "Shipper đã đến giao hàng, bạn hãy nhận hàng và thưởng thức nó. Sau đó đừng quên đánh giá ở đây nha ><");

        Notification notification = new NotificationCompat.Builder(this, com.ute.food_express_app.actvitie.App.CHANNEL_ID)
                .setSmallIcon(R.drawable.restaurant)
                .setCustomContentView(collapsedView)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setContentIntent(resultPendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager != null){
            notificationManager.notify(1, notification);
        }
        else{
            Log.d("hang", "pushNotification: " + notification);
        }
    }

    private int getNotificationId(){
        return (int) new Date().getTime();
    }


}
