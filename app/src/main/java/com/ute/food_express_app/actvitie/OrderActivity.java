package com.ute.food_express_app.actvitie;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObservable;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ute.food_express_app.R;
import com.ute.food_express_app.adapter.CartFoodAdapter;
import com.ute.food_express_app.model.Customer;
import com.ute.food_express_app.model.Food;
import com.ute.food_express_app.model.MyFood;
import com.ute.food_express_app.model2.Discount;
import com.ute.food_express_app.model2.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OrderActivity extends AppCompatActivity {

    private Button btnOrder;
    private ImageButton btnReturn;
    private Button btnApply;
    private TextView txtName, txtPhone, txtOrderAddress, txtSumOfPrice,
            txtDeliveryCost, txtTotalCost, txtDiscount, txtSale;
    private EditText edtDiscount;
    List<Food> foodList;
    User user;
    int userId;
    int orderId;
    Bundle bundle = new Bundle();
    DecimalFormat formatter = new DecimalFormat("###,###,###");
    Discount discount;

    static final int DELIVERY_COST = 20000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_order);

        // order food
        Intent intent = getIntent();
        foodList = (List<Food>) intent.getSerializableExtra("foodList");
        Log.v("Duong food list: ", foodList.toString());

        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        userId = sharedPreferences.getInt("customerId", 1);

        loadUser();

        RecyclerView rcvOrderFood = findViewById(R.id.rcvOrderFood);
        CartFoodAdapter orderFoodAdapter = new CartFoodAdapter(this, (ArrayList<Food>) foodList);
        rcvOrderFood.setAdapter(orderFoodAdapter);
        LinearLayoutManager verticalLayout = new LinearLayoutManager(OrderActivity.this, LinearLayoutManager.VERTICAL,
                false);
        rcvOrderFood.setLayoutManager(verticalLayout);

        // mapping
        mapping();

        setValue();

        // btn Order click
        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveOrder();
                Intent intent = new Intent(OrderActivity.this, MapActivity.class);
                bundle.putSerializable("foodList", (Serializable) foodList);
                bundle.putSerializable("user", (Serializable) user);

                if (discount != null) {
                    bundle.putInt("discount", discount.getValue());
                    bundle.putString("code", discount.getCode());
                }

                final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        intent.putExtra("order", bundle);
                        startActivity(intent);
                    }
                }, 2000);
            }
        });

        // btnReturn click
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderActivity.this, RestaurantDetailActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });


        // btn apply set onclick
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = edtDiscount.getText().toString();
                edtDiscount.clearFocus();
                if (code != "") {
                    getDiscount(code);
                } else {
                    Toast.makeText(OrderActivity.this, "Code is invalid !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void mapping() {
        btnOrder = (Button) findViewById(R.id.btnOrder);
        btnReturn = (ImageButton) findViewById(R.id.btnReturn);
        btnApply = findViewById(R.id.btnApply);

        txtName = findViewById(R.id.txtName);
        txtPhone = findViewById(R.id.txtPhone);
        txtSale = findViewById(R.id.txtSale);
        txtOrderAddress = findViewById(R.id.txtOrderAddress);

        edtDiscount = findViewById(R.id.edtDiscount);
        txtDiscount = findViewById(R.id.txtDiscount);

    }

    public void setValue() {
        txtSumOfPrice = findViewById(R.id.txtSumOfPrice);
        txtDeliveryCost = findViewById(R.id.txtShippingCost);
        txtTotalCost = findViewById(R.id.txtTotalCost);

        txtSumOfPrice.setText(formatter.format(getSumOfPrice(foodList)));
        txtDeliveryCost.setText(formatter.format(DELIVERY_COST));
        txtTotalCost.setText(formatter.format(getSumOfPrice(foodList) + DELIVERY_COST));
    }

    public int getSumOfPrice(List<Food> foodList) {
        int priceTotal = 0;
        for (Food food : foodList) {
            priceTotal += food.getQuantityOrder() * food.getPrice();
        }

        return priceTotal;
    }

    public static Date addMinutesToDate(Date date, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);
        return calendar.getTime();
    }

    public int getDiscountPrice(Discount discount) {
        return 0;
    }

    public void pushNotification(){
        RemoteViews collapsedView = new RemoteViews(getPackageName(),
                R.layout.order_notification_layout);

        Intent resultIntent = new Intent(this, OrderDetailActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(getNotificationId(), PendingIntent.FLAG_UPDATE_CURRENT);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, "Đặt đơn hàng thành công");
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, "Đơn hàng của bạn đã được đặt.Vui lòng đợi trong giây lát.");

        Notification notification = new NotificationCompat.Builder(this, com.ute.food_express_app.actvitie.App.CHANNEL_ID)
                .setSmallIcon(R.drawable.restaurant)
                .setCustomContentView(collapsedView)
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setContentIntent(resultPendingIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager != null){
            notificationManager.notify(1, notification);
        }
        else{
            Log.d("hang", "pushNotification: " + notification);
        }
    }

    private int getNotificationId(){
        return (int) new Date().getTime();
    }

    public void loadUser() {
        user = new User();
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/customer/" + userId)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject object = response.getJSONObject(0);

                            user.setName(object.getString("fullName"));
                            user.setPhone(object.getString("phone"));
                            user.setNumber(object.getInt("number"));
                            user.setStreet(object.getString("street"));

                            txtName.setText(user.getName());
                            txtPhone.setText("0" + user.getPhone());
                            txtOrderAddress.setText(user.getStreet());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.v("Error Duongggggggggg", anError.toString());
                    }
                });
    }

    public void saveOrder() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-MM hh:mm");
        Date createdTime = new Date();

        Date deliveryTime = addMinutesToDate(new Date(), 15);
        Date paymentTime = addMinutesToDate(new Date(), 20);

        int cost = 0;
        if (discount != null) {
            cost = 20000 - discount.getValue();
        } else {
            cost = 20000;
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("createdTime", format.format(createdTime));
            jsonObject.put("note", "");
            jsonObject.put("deliveryCost", String.valueOf(cost));
            jsonObject.put("deliveryTime", format.format(deliveryTime));
            jsonObject.put("paymentTime", format.format(paymentTime));
            jsonObject.put("customerId", String.valueOf(userId));
            jsonObject.put("paymentId", "1");
            jsonObject.put("orderStatus", "5");
            jsonObject.put("discountId", "1");
            jsonObject.put("reasonId", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.v("Duong 1111111", jsonObject.toString());

        AndroidNetworking.post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/order/createOrder")
                .addJSONObjectBody(jsonObject)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("Duong success: ", response.toString());
                        getLastesOrder();
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(OrderActivity.this, "Không hợp lệ", Toast.LENGTH_SHORT).show();
                        Log.v("Duong fail: ", error.toString());
                    }
                });
    }

    public void getLastesOrder() {
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/order")
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject object = response.getJSONObject(response.length() - 1);

                            orderId = object.getInt("id");
                            bundle.putInt("orderId", orderId);

                            Log.v("Duong5555555555", String.valueOf(orderId));

                            for (Food food : foodList) {
                                saveOrderDetai(food, orderId);
                            }

                            final Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    pushNotification();
//                                    Toast.makeText(OrderActivity.this, "Order successfully", Toast.LENGTH_SHORT).show();
                                }
                            }, 1000);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.v("Error Duongggggggggg", anError.toString());
                    }
                });
    }

    public void saveOrderDetai(Food food, int orderId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("foodId", String.valueOf(food.getId()));
            jsonObject.put("orderId", String.valueOf(orderId));
            jsonObject.put("quantity", String.valueOf(food.getQuantityOrder()));
            jsonObject.put("price", String.valueOf(food.getPrice() * food.getQuantityOrder()));

            Log.v("Duong5555555", jsonObject.toString());

            Log.v("Duong55555111", String.valueOf(orderId));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking
                .post("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/orderDetail/createOrderDetail")
                .addJSONObjectBody(jsonObject)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("Duong success: ", response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(OrderActivity.this, "System error", Toast.LENGTH_SHORT).show();
                        Log.v("Duong fail: ", error.toString());
                    }
                });

    }

    public void getDiscount(String code) {
        AndroidNetworking.get("http://ec2-3-18-213-174.us-east-2.compute.amazonaws.com:3000/api/discount/code/" + code)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        if (jsonArray.length() <= 0) {
                            Toast.makeText(OrderActivity.this, "Code is invalid !!!", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                JSONObject response = jsonArray.getJSONObject(0);

                                discount = new Discount();
                                discount.setId(response.getInt("id"));
                                discount.setValue(response.getInt("valueDiscount"));
                                discount.setCode(response.getString("code"));

                                Log.v("Duong discountttttt: ", discount.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(OrderActivity.this, "Mã không hợp lệ !!!", Toast.LENGTH_SHORT).show();
                            }

                            if (discount != null) {
                                txtDiscount.setText(formatter.format(discount.getValue()));
                                txtTotalCost.setText(formatter
                                        .format(getSumOfPrice(foodList) + DELIVERY_COST - discount.getValue()));
                                txtSale.setText("Giảm giá (" + code + ")");
                                Toast.makeText(OrderActivity.this, "Áp dụng mã thành công", Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(OrderActivity.this, "Mã không hợp lệ", Toast.LENGTH_SHORT).show();
                        Log.v("Duong discount fail: ", error.toString());
                    }
                });

    }
}
