package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ute.food_express_app.R;

public class YourLocation3Activity extends AppCompatActivity {
    ImageButton btn_return;
    EditText edit_seach;
    TextView txt_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_your_location3);

        btn_return = (ImageButton) findViewById(R.id.btn_return);
        edit_seach = (EditText) findViewById(R.id.edit_seach);
        txt_address = (TextView) findViewById(R.id.textView_adress);

        Intent intent1 = getIntent();
        String value1 = intent1.getStringExtra("Key_1");
        edit_seach.setText(value1);

        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(YourLocation3Activity.this, YourLocation2Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        txt_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(YourLocation3Activity.this, MainActivity2.class);
                startActivity(intent1);
            }
        });
    }
}