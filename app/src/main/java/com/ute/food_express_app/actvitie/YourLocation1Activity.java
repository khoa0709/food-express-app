package com.ute.food_express_app.actvitie;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.content.Intent;

import com.ute.food_express_app.R;

public class YourLocation1Activity extends AppCompatActivity {
    Button btnInput;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_your_location1);

        btnInput = (Button) findViewById(R.id.btn_input);
        Intent i = getIntent();
        int cusId = i.getIntExtra("id", 0);

        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(YourLocation1Activity.this, LocationActivity.class);
                intent.putExtra("id", cusId);
                startActivity(intent);
            }
        });

    }
}